---
name: "Exploring the Internet"
short: ""
startDate: 'October 20 1994 17:30'
online: false
location: "MC 3009"
---

### Need something to do between assignments/beers?

Did you know that your undergrad account at Waterloo gives you access to the world's largest computer network? With thousands of discussion groups, gigabytes of files to download, multimedia information browsers, even on-line entertainment?

The resources available on the Internet are vast and wondrous, but the tools for navigating it are sometimes confusing and arcane. In this hands-on tutorial you will get the chance to get your feet wet with the world's most mind-bogglingly big computer network, the protocols and programs used, and how to use them responsibly and effectively.
