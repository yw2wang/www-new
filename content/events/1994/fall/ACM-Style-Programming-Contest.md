---
name: "ACM-Style Programming Contest"
short: ""
startDate: 'October 15 1994 11:00'
online: false
location: "MC 3022"
---

### Big Money and Prizes!

So you think you're a pretty good programmer? Pit your skills against others on campus in this triannual event! Contestants will have three hours to solve five programming problems in either C or Pascal.

Last fall's winners went on to the International Finals and came first overall! You could be there, too!
