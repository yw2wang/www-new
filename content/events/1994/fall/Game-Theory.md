---
name: "Game Theory"
short: ""
startDate: 'November 02 1994 17:30'
online: false
location: "MC 2038"
---

### From the Minimax Theorem, through Alpha-Beta, and beyond...

This will be a discussion of the pitfalls of using mathematics and algorithms to play classical board games. Thorough descriptions shall be presented of the simple techniques used as the building blocks that make all modern computer game players. I will use tic-tac-toe as a control for my arguments. Other games such as Chess, Othello and Go shall be the be a greater measure of progress; and more importantly the targets of our dreams.

To enhance the discussion of the future, Barney Pell's Metagamer shall be introduced. His work in define classes of games is important in identifying the features necessary for analysis.
