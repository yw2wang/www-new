---
name: 'Co-op Masterclass'
short: 'Learn how to optimize WaterlooWorks, create a resume that gets your foot in the door, and polish your interview skills to show that you’re the best fit for the role!'
startDate: 'January 12 2023 19:00'
endDate: 'January 12 2023 21:00'
online: false
location: 'DC 1351'
poster: 'images/events/2023/winter/Bootcamp-Coop-Masterclass.jpg'
---

⭐ Take your co-op searching skills to the next level in Bootcamp’s Co-op Masterclass on optimizing WaterlooWorks, creating a resume that gets your foot in the door, and polishing your interview skills to show that you’re the best fit for the role!

📋 We’ve invited a few upper-year CS students – co-op veterans ✨ – to speak from their job-hunting experiences and share their tips and strategies for success! We’ll end off the event with an open Q&A session so you can get any other questions you have answered!

📅 Co-op Masterclass will be taking place on Thursday, January 12th from 7-9 PM at DC 1351.

🤩 Especially if you’re in your first or even second co-op search, Co-op Masterclass is an amazing opportunity to get the insight and advice you need to boost your chances of getting your dream co-op placements! We hope to see you there!!
