---
name: 'BOT Bonfire'
short: 'Join CSC and WiCS in kicking off the term with a bang! Come out to have some fun with a night of amazing games, speed friending and smore’s!'
startDate: 'May 23 2023 19:00'
endDate: 'May 23 2023 21:00'
online: false
location: 'Columbia Lake Fire Pit'
poster: 'images/events/2023/spring/bonfire_BOT.png'
---

☀️ Kick off the summer term with CSC’s BOT Bonfire event! Are you interested in attending upcoming CSC events? Want to meet others in the CS community? Come to our beginning of term event!

🎉 Come join us for a night of fun games, a painting station, speed friending, as well as snacks including smores, pizza, drinks, and freezies! Be sure to dress warm, bring bug spray, and bring your own instrument, frisbee, skewer or blanket!

📆 When? Tuesday May 23rd 2023 at 7:00 - 9:00pm EST, in Columbia Lake Fire Pit.