---
name: 'Spring 2023 Elections'
short: 'The CS Club will be holding elections for the Spring 2023 term on Wednesday, May 10 at 6:00PM in MC 2034.'
startDate: 'May 10 2023 18:00'
endDate: 'May 10 2023 20:00'
online: false
location: 'MC 2034'
poster: 'images/events/2023/spring/Spring-2023-Elections.png'
---

🗳 The CS Club will be holding elections for the Spring 2023 term on Wednesday, May 10 at 6:00PM in MC 2034.

👉 Come to learn more about CSC, sign up for membership, and vote on our new execs! The president, vice-president, assistant vice-president, and treasurer will be elected, and the sysadmin will be appointed. Furthermore, we will vote on some changes to the constitution outlined here: https://csclub.ca/constitutionChanges

✋ If you'd like to run for any of these positions or nominate someone, you can send an email to cro@csclub.uwaterloo.ca, or present them in-person to the CRO, Ivy Lei, or write your name on the whiteboard in the CSC office (MC 3036). Nominations will close on May 9 at 6PM. Nominees will be reached out to for their platforms.

❗If you are unable to attend the elections in person, please email cro@csclub.uwaterloo.ca in order to request an absentee ballot and vote remotely.

❓ If you have any questions about elections, please email cro@csclub.uwaterloo.ca.
