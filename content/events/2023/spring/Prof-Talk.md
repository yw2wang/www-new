---
name: "'Attention to Failure: There's No Such Thing as Typical Innovation' with Professor Gavin Duggan"
short: 'Join Professor Gavin Duggan on July 19th, as he unpacks how doing things differently and embracing failure can make life harder, yet more interesting and more productive.'
startDate: 'July 19 2023 17:00'
endDate: 'July 19 2023 19:00'
online: false
location: 'E7 Ideas Clinic'
poster: 'images/events/2023/spring/Gavin_Speaker_Poster.png'
---

Ever had an existential crisis on whether or not you're taking the right path? Fear no longer!

CSC and EngSoc is joined by Professor Gavin Duggan on July 19th, as he unpacks how doing things differently and embracing failure can make life harder, yet more interesting and more productive.

📆 July 19th, 5-7pm

📍 Ideas Clinic, E7

☕️ Refreshments will be served for all CSC members!

