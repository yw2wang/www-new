---
name: 'Escape MC Manor!'
short: 'Get into the spooky spirit with our Halloween-themed escape adventure in MC! Teams of four will "trick or "treat" across different challenges! Get ready to solve riddles, code through LeetCode challenges, and race to the ending room for a shot at CSC swag!'
startDate: 'October 31 2023 18:00'
endDate: 'October 31 2023 20:00'
online: false
location: 'MC 4020'
poster: 'images/events/2023/fall/1707443339373--Escape-MC-Manor.png'
---

🎃 Can you escape the MC Manor? Join us for a spooky night of challenges, tricks, and treats! Complete 4 distinct challenges with your friends and escape from MC first to earn some awesome CSC swag!

📢 Sign up via the link in our bio to register for the event either individually or with a team of friends! Those who sign up individually will be assigned teams on the day of the event.

📆 When? October 31th, 2023 at 6:00 - 8:00pm EST

📍 Where? MC 4020
