---
name: 'Neetcode Talk'
short: 'Neetcode will be talking about leetcode and coding interviews, as well as his career journey to Google'
startDate: 'October 27 2023 18:00'
endDate: 'October 27 2023 19:30'
online: false
location: 'RCH 301'
poster: 'images/events/2023/fall/1707680876581--neetcode_talk_poster.png'
registerLink: 'https://docs.google.com/forms/d/e/1FAIpQLSdns-sfeo35nweshqUSam25PyBaZNFBQ0Yg9XFwV6rWRB8CCA/viewform?usp=sharing '
---

🚀 Get ready to level up your Leetcode skills! Join our exclusive Q&A session with the founder of NeetCode.io and ex-Amazon, Google software engineer!

👨‍💻Ask anything about school, life, career, or Leetcode. Food and snacks will be provided to UW CSC and GDSC members only.

📢 Sign up with link in bio!

📆 When? October 27th, 2023 at 6:00 - 7:30pm ET

📍 Where? RCH 301
