---
name: 'By-Elections'
short: 'As there are vacancies in the executive council, there will be by-election on May 22nd. The following positions are open for election:- Treasurer- Secretary<!-- -->The executive are also looking for people who may be interested in the following positions:- Systems Administrator- Office Manager- Librarian<!-- -->'
startDate: 'May 22 2015 16:00'
online: false
location: 'MC 3001 (Coomfy)'
---

As there are vacancies in the executive council, there will be by-election on May 22nd. The following positions are open for election:

- Treasurer
- Secretary

<!-- -->

The executive are also looking for people who may be interested in the following positions:

- Systems Administrator
- Office Manager
- Librarian

<!-- -->

