---
name: 'Code Party 0'
short: 'The first code party of Winter 2015, and we have something a litle different this time. We''re running a Code Retreat (coderetreat.org) with Boltmade. The result of this is that you will be able to do a coding challenge, wherein you implement Rule 110 (like the Game of Life). Of course, if you want to work on whatever you can do that as well. Delicious free food, but RSVP! [bit.ly/code-party-0](<https://bit.ly/code-party-0>)'
startDate: 'February 27 2015 18:00'
online: false
location: 'EV3 1408'
---

The first code party of Winter 2015, and we have something a litle different this time. We're running a Code Retreat (coderetreat.org) with Boltmade. The result of this is that you will be able to do a coding challenge, wherein you implement Rule 110 (like the Game of Life). Of course, if you want to work on whatever you can do that as well. Delicious free food, but RSVP! [bit.ly/code-party-0](<https://bit.ly/code-party-0>)

