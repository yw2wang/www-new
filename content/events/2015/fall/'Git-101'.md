---
name: '''Git 101'''
short: 'Learn how to use Git properly in an exciting talk by Charlie Wang!'
startDate: 'November 19 2015 07:00'
online: false
location: 'MC 4020'
---

git init, git add, git commit, git 'er done!

In Git 101, Charlie Wang will convince you to use Git for your projects and show you a high level overview of how to use it properly.

This talk is recommended for CS 246 students.

Come for the tutorial, stay for the bad jokes.

