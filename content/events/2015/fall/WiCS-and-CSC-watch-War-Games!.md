---
name: 'WiCS and CSC watch War Games!'
short: 'WiCS and CSC are watching War Games in the Comfy lounge.'
startDate: 'November 27 2015 19:30'
online: false
location: 'MC Comfy'
---

WiCS and CSC are watching War Games in the Comfy lounge.

War Games is this movie where these kids phone a computer and then the computer wants to nuke things. Cold war stuff. Nowadays computers won't let you do that, you have to SSH in instead.

We're bringing food. Gluten-free, vegetarian options available. Sandwiches, drinks, and popcorn!

Everyone welcome! Stop by!

