---
name: 'The Free Software Movement and the GNU/Linux Operating System'
short: 'A talk by Richard M. Stallman (RMS) **[CANCELLED]**'
startDate: 'April 11 2007 15:30'
online: false
location: 'Hagey Hall'
---

Richard Stallman has cancelled his trip to Canada.

