---
name: 'Virtual Reality, Real Law: The regulation of Property in Video Games'
short: 'Susan Abramovitch'
startDate: 'September 25 2007 13:30'
online: false
location: 'DC 1302'
---

This talk is run by the School of Computer Science

How should virtual property created in games, such as weapons used in games like Mir 3 and real estate or clothing created or acquired in games like Second Life, be treated in law. Although the videogaming industry continues to multiply in value, virtual property created in virtual worlds has not been formally recognized by any North American court or legislature. A bridge has been taking shape from gaming's virtual economies to real world economies, for example, through unauthorized copying of designer clothes sold on Second Life for in-game cash, or real court damages awarded against deletion of player-earned swords in Mir 3. The trading of virtual property is important to a large number of people and property rights in virtual property are currently being recognized by some foreign legal bodies.

Susan Abramovitch will explain the legal considerations in determining how virtual property can or should be governed, and ways it can be legally similar to tangible property. Virtual property can carry both physical and intellectual property rights. Typically video game developers retain these rights via online agreements, but Ms. Abramovitch questions whether these rights are ultimately enforceable and will describe policy issues that may impact law makers in deciding how to treat virtual property under such agreements.

