---
name: 'Alt+Tab Talks'
short: 'Come watch (or give!) interesting short talks by CS Club members.	Talks include "Dynamic programming as path finding", "What is a landing page", "Subsurface scattering" and "How to compute on a GPU", but more are welcome (email [tghume@csclub.uwaterloo.ca](<mailto:tghume@csclub.uwaterloo.ca>))! Click the link to the event detail page for more info.'
startDate: 'November 09 2017 18:00'
online: false
location: 'MC 4059'
---

Come watch (or give!) interesting short talks by CS Club members.	Talks include "Dynamic programming as path finding", "What is a landing page", "How to compute on a GPU" and "Subsurface scattering", but more are welcome (email [tghume@csclub.uwaterloo.ca](<mailto:tghume@csclub.uwaterloo.ca>))! There will be food.

Each talk can be 5-15 minutes long on any computer-related topic of interest.	If you're interested in giving a talk (please do!) email [tghume@csclub.uwaterloo.ca](<mailto:tghume@csclub.uwaterloo.ca>).

