---
name: 'Fall 2017 Elections'
short: 'The Computer Science Club will be holding elections for the Spring 2017 President, Vice-President, Secretary and Treasurer. Additionally, the Systems Administrator, Office Manager and Librarian, CTF Club Liaison and Fridge Person will be appointed.'
startDate: 'September 15 2017 18:00'
online: false
location: 'MC Comfy Lounge'
---

The Computer Science Club will be holding elections for the Fall 2017 term on Friday, September 15th at 6:00pm in the MC Comfy Lounge (MC 3001).

The following positions will be elected: President, Vice-President, Treasurer and Secretary. The following positions will be appointed: Systems Administrator (to be ratified at the meeting), Office Manager and Librarian, CTF Club Liaison and Fridge Person (the exact name of this position is still to be determined). Additionally, we will be looking for members to join the Programme Committee.

If you would like to run or nominate someone for any of the elected positions, you can put your name in a special box in the CSC office (MC 3036/3037) or by sending an email to the Chief Returning Officer (Felix) at [cro@csclub.uwaterloo.ca](<mailto:cro@csclub.uwaterloo.ca>). Please note that executive positions are restricted to MathSoc social members. We welcome the participation of first years.

Nominations will close at 6:00pm on Thursday, September 14th (24 hours prior to the start of elections). After that time, a list of current nominations will be sent out by email. It will also be available on the whiteboard in the office and at [https://csclub.uwaterloo.ca/elections](<https://csclub.uwaterloo.ca/elections>). Voting will be done in a heads-down, hands-up manner and is restricted to MathSoc social members. A full description of the roles and the election procedure are listed in our Constitution, available at [ https://csclub.uwaterloo.ca/about/constitution ](<https://csclub.uwaterloo.ca/about/constitution>). Any questions related to the election can be directed to [cro@csclub.uwaterloo.ca](<mailto:cro@csclub.uwaterloo.ca>).

