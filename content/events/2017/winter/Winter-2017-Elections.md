---
name: 'Winter 2017 Elections'
short: 'The Computer Science Club will be holding elections for the Winter 2017 President, Vice-President, Secretary and Treasurer. Additionally, the Systems Administrator, Office Manager and Librarian will be appointed.'
startDate: 'January 12 2017 18:00'
online: false
location: 'MC Comfy Lounge'
---

The Computer Science Club will be holding elections for the Winter 2017 President, Vice-President, Secretary and Treasurer. Additionally, the Systems Administrator, Office Manager and Librarian will be appointed.

The following positions will be elected: President, Vice-President, Treasurer and Secretary. The following positions will be appointed: Systems Administrator (to be ratified at the meeting), Office Manager and Librarian. Additionally, we will be looking for members to join the Programme Committee. The nominees for the four elected positions are:

- President
- Vice President
- Treasurer
- Secretary

<!-- -->

Voting will be done in a heads-down, hands-up manner and is restricted to MathSoc social members. We use approval voting; for each position, you may vote for any subset of the candidates. If you wish to vote but will not be attending the election, you may send an absentee ballot indicating which candidate(s) you wish to vote for, for each position. This ballot must be sent to cro@csclub.uwaterloo.ca from your @uwaterloo.ca or @csclub.uwaterloo.ca email address. A full description of the roles and the election procedure are listed in our Constitution, available at https://csclub.uwaterloo.ca/about/constitution.

