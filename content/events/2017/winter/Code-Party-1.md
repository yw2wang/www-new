---
name: 'Code Party 1'
short: 'Come code with us, eat some food, do some things. Personal projects you want to work on? Homework projects you need to finish? Or want some time to explore some new technology and chat about it? You can join us at Code Party 1 and do it, with great company and great food. Come any time after 5pm, but if you come earlier your food preferences are more likely to be accounted for, and there''s more time for coding!'
startDate: 'April 05 2017 17:00'
online: false
location: 'STC 0060'
---

Come code with us, eat some food, do some things.

Personal projects you want to work on? Homework projects you need to finish? Or want some time to explore some new technology and chat about it? You can join us at Code Party 1 and do it, with great company and great food.

Come any time after 5pm, but if you come earlier your food preferences are more likely to be accounted for, and there's more time for coding!

