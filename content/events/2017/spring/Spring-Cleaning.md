---
name: 'Spring Cleaning'
short: 'Join us for Spring Cleaning!'
startDate: 'July 15 2017 10:00'
online: false
location: 'CSC Office'
---

We will be conducting our Spring Cleaning on Saturday, July the 15th @	10:00am. We'll be clearing out some junk, mopping the floors, dusting	off the tables/shelves, and generally tidying up the place. The more	help we can get the better! If you would like to lend a hand, just come	over to the office this weekend.

