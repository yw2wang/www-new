---
name: 'Alt-Tab: S17'
short: 'Join us for food and interesting member talks!'
startDate: 'July 18 2017 17:00'
online: false
location: 'MC4040'
---

CSC's Alt-Tab is back! Join us for food and interesting member talks. The current lineup includes:

- Ifaz Kabir: "The comment that took Stack Exchange down and the algorithm that could have saved them"
- Fatema Boxwala: "Manic PXE Dream Servers"
- Charlie Wang: TBA (Something About Typed Racket)
- Sean Harrap: "Register Allocation With Graphs"
- Bryan Coutts: "Vehicle Routing"
- Reila Lee: TBA

<!-- -->

