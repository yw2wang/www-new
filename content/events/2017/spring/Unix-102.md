---
name: 'Unix 102'
short: 'Come gain some more in-depth knowledge or some less well-known tips and tricks for using the command line.'
startDate: 'June 01 2017 18:00'
online: false
location: 'MC 3003'
---

Finished the bash unit in CS246 and still don't see what's great about Unix? Want to gain some more in-depth knowledge, or some less well-known tips and tricks for using the command line? Unix 102 is the event for you! Fatema is "kind of successful" and "knows things about Unix" and you can be too! Topics covered will be: users, groups and permissions, ez string manipulation, additional skills, tips and tricks.

