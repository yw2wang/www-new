---
name: 'Unix 101'
short: 'Need to use the Unix environment for a course, want to overcome your fears of the command line, or just curious? Attend the first installment in the CSC''s popular series of Unix tutorials to learn the basics of the shell and how to navigate the unix environment. By the end of the hands on workshop you will be able to work efficiently from the command line and power-use circles around your friends.'
startDate: 'September 29 2010 16:30'
online: false
location: 'MC3003'
---

Need to use the Unix environment for a course, want to overcome your fears of the command line, or just curious? Attend the first installment in the CSC's popular series of Unix tutorails to learn the basics of the shell and how to navigate the unix environment. By the end of the hands on workshop you will be able to work efficiently from the command line and power-use circles around your friends.

