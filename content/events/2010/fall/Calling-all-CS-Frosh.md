---
name: 'Calling all CS Frosh'
short: 'Come meet and greet your professors, advisors, and the heads of the school. Talk to the CSC executive and other upper year students about CS at Waterloo. Free food and beverages will also be available, so there is really no excuse to miss this.'
startDate: 'September 23 2010 16:30'
online: false
location: 'DC1301 (The Fishbowl)'
---

Come meet and greet your professors, advisors, and the heads of the school. Talk to the CSC executive and other upper year students about CS at Waterloo. Free food and beverages will also be available, so there is really no excuse to miss this.

