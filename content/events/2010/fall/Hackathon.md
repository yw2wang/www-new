---
name: 'Hackathon'
short: 'Come join the CSC for a night of code, music with only 8 bits, and comradarie. We will be in the C&D Lounge from 7pm until 7am working on personal projects, open source projects, and whatever else comes to mind. If you''re interested in getting involved in free/open source development, some members will be on hand to guide you through the process.'
startDate: 'November 05 2010 19:00'
online: false
location: 'CnD Lounge (MC3002)'
---

Come join the CSC for a night of code, music with only 8 bits, and comradarie. We will be in the C&D Lounge from 7pm until 7am working on personal projects, open source projects, and whatever else comes to mind. If you're interested in getting involved in free/open source development, some members will be on hand to guide you through the process.

