---
name: 'Bowling'
short: 'The CSC is going bowling. $9.75 for shoes and two games. The bowling alley serves fried food and beer. Join us for some or all of the above'
startDate: 'March 06 2010 17:00'
online: false
location: 'Waterloo Bowling Lanes'
---

The CSC is going bowling. $9.75 for shoes and two games. The bowling alley serves fried food and beer. Join us for some or all of the above

