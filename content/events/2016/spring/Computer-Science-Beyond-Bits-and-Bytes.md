---
name: 'Computer Science: Beyond Bits and Bytes'
short: 'Gladimir Baranoski is an Associate Professor at the School of Computer Science, in the	Natural Phenomena Simulation Group. He will be giving a talk on underappreciated	facets of computer science and its connections to other disciplines.'
startDate: 'July 14 2016 18:30'
online: false
location: 'MC 2034'
---

Talk Abstract: Computer science is often perceived to be confined to	traditional areas such as operating systems, programming languages,	compilers and so on. Viewed in this context, one’s professional future	in this field seems to be directly linked to the accumulation of knowledge and practical experience in these areas. Although their importance is	undeniable, it is also possible, and highly recommended, to expand one’s	horizons. In this talk, we are going to informally look at ubiquitous,	albeit sometimes underappreciated, facets of computer science and its	synergistic connections to other disciplines. We are also going to discuss	how creativity and serendipity can impact one’s career and lead to tangible contributions in physical and life sciences.

