---
name: 'WiCS and CSC Go Outside!'
short: 'Join us at BMH Green for a night outdoors with fellow people in Computer Science!	There will be ice cream and board games and frisbees and maybe some water guns.	Bring your friends!'
startDate: 'July 11 2016 19:00'
online: false
location: 'BMH Green'
---

Join us at BMH Green for a night outdoors with fellow people in Computer Science!	There will be ice cream and board games and frisbees and maybe some water guns.	Bring your friends!

