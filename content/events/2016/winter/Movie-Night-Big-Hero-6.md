---
name: 'Movie Night: Big Hero 6'
short: 'Movie Night! Come watch "Big Hero 6" with the CSC!'
startDate: 'February 10 2016 18:30'
online: false
location: 'MC Comfy'
---

Come watch "Big Hero 6" with the Computer Science Club this wednesday the 10th at 6:30 PM in the MC Comfy Lounge. Why "Big Hero 6"? It's an award-winning animated Disney movie involving an inflatable robot fighting evil in "San Frasokyo". Enough said.

