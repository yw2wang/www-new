---
name: 'Richard Mann Prof Talk'
short: 'Professor Richard Mann will be giving a talk, titled "Open Source Software for Sound Measurement and Analysis". He will be presenting information about his new course, CS 489, Computational Sound, which will be running in Winter 2017.'
startDate: 'November 21 2016 18:15'
online: false
location: 'MC 4063'
---

Professor Richard Mann will be giving a talk, titled "Open Source Software for Sound Measurement and Analysis". He will be presenting information about his new course, CS 489, Computational Sound, which will be running in Winter 2017. The abstract for this talk is below. 





The most common problem in acoustics is to measure the frequency response of an (expensive!) listening room. While specifications exist for the amplifiers, speakers, etc, each system must be still evaluated individually, since the frequency response depends on the direct sound from the speaker(s), the listener position and the reverberation of the room. The user may spend considerable time adjusting the speaker placement, the system equalization, and possibly treating the room to get the best response.

There are several commercial and freeware applications for this task, some of which are very good. However, to learn the methods the user must understand the processing involved.

The purpose of this talk is to present an open source solution. Our system is based on a very few lines of code, written in GNU Octave, a Matlab(r) workalike that runs under Linux, Windows and Mac.

The program works by playing a known test signal, such a tone, or some kind of noise source out of the sound card into the system. The system is measured by comparing driving signal to that measured by a microphone in the room. The frequency response is computed using the Discrete Fourier Transform (DFT).

This is joint work with Prof. John Vanderkooy, Physics, University of Waterloo.

