---
name: 'Bringing OOP Best Practices to the World of Functional Programming'
short: 'The CSC will have its first talk of the term this Thursday, October 6th. UW alumna and CSC member Elana Hashman will be giving a talk on using functional programming languages (like Racket!) in industry, and how some concepts from the more common object-oriented paradigm are translated to the functional paradigm. The abstract for the talk is below.'
startDate: 'October 06 2016 18:00'
online: false
location: 'MC 4021'
---

I transitioned from writing software in imperative, object-oriented (OO) programming languages to doing functional programming (FP) full-time, and you can do it, too! In this talk, I'll make a case for using FP for real-world development, cover some cases where common FP language features substitute for design patterns and OOP structure, and provide some examples of translating traditional OO design patterns into functional code.

