---
name: 'Code Party'
short: 'Come code with us, eat some food, do some things. Personal projects you want to work on? Homework projects you need to finish? Or want some time to explore some new technology and chat about it? You can join us at Code Party and do it, with great company and great food.'
startDate: 'November 16 2016 18:30'
online: false
location: 'M3 1006'
---

Come code with us, eat some food, do some things. Personal projects you want to work on? Homework projects you need to finish? Or want some time to explore some new technology and chat about it? You can join us at Code Party and do it, with great company and great food.

