---
name: 'LaTeX: Beautiful Mathematics'
short: 'LaTeX => fun'
startDate: 'February 20 2003 18:30'
online: false
location: 'MC1085'
---

It is widely acknowledged that the best system by which to typeset beautiful mathematics is through the T<small>E</small>

 typesetting system, written by Donald Knuth in the early 1980s.

In this talk, I will demonstrate L<sup><small>A</small></sup>

T<small>E</small>

X and how to typeset elegant mathematical expressions.

