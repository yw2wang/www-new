---
name: 'XSLT'
short: 'Transforming your documents'
startDate: 'March 20 2003 18:30'
online: false
location: 'MC1085'
---

XSLT is the <q>eXtended Stylesheet Language Transformations,</q>

 a language for transforming XML documents into other XML documents.

XSLT is used to manipulate XML documents into other forms: a sort of glue between data formats. It can turn an XML document into an XHTML document, or even an HTML document. With a little bit of hackery, it can even be convinced to spit out non-XML conforming documents.

