---
name: 'Real-Time Graphics Compilers'
short: 'Sh is a GPU metaprogramming language developed at the UW Computer Graphics Lab'
startDate: 'October 22 2003 16:30'
online: false
location: 'MC4061'
---

Sh is a GPU metaprogramming language developed at the University of Waterloo Computer Graphics Lab. It allows graphics programmers to write programs which run directly on the GPU (Graphics Processing Unit) using familiar C++ syntax. Furthermore, it allows metaprogramming of such programs, that is, writing programs which generate other programs, in an easy and natural manner.

This talk will give a brief overview of how Sh works, the design of its intermediate representation and the (still somewhat simplistic) optimizer that the current reference implementation has and problems with applying traditional compiler optimizations.

Stefanus Du Toit is an undergraduate student at the University of Waterloo. He is also a Research Assistant for Michael McCool from the University of Waterloo Graphics Lab. Over the Summer of 2003 Stefanus reimplemented the Sh reference implementation and designed and implemented the current Sh optimizer.

