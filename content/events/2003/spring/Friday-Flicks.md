---
name: 'Friday Flicks'
short: ' SIGGRAPH Electronic Theatre Showing '
startDate: 'June 27 2003 14:30'
online: false
location: 'DC1302'
---

SIGGRAPH is the ACM's Special Interest Group for Graphics and simultaneously the world's largest graphics conference and exhibition, where the cutting edge of graphics research is presented every year.

With support from UW's Computer Graphics Lab, the CSC invites you to capture a glimpse of SIGGRAPH 2002. We will be presenting the Electronic Theatre showings from 2002, demonstrating the best of the animated, CG-produced movies presented at SIGGRAPH.

Don't miss this free showing!

