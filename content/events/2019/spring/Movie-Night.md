---
name: 'Movie Night'
short: 'The Computer Science Club will be holding a joint event with Women in Computer Science on	Tuesday, July 9th at MC Comfy at 7:00 PM.'
startDate: 'July 09 2019 19:00'
online: false
location: 'MC Comfy'
---

Come hang out and watch The Lego Batman Movie with us! There will popcorn and pizza!

