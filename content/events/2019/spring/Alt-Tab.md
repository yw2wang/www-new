---
name: 'Alt-Tab'
short: 'The Computer Science Club will be holding a joint event with Women in Computer	Science on Thursday, July 18th at MC 5417 at 6:00 PM.'
startDate: 'July 18 2019 18:00'
online: false
location: 'MC 5417'
---

Alt-Tab is back, and this time being co-hosted with WiCS. Come listen to lightning tech talks by fellow students that will knock your metaphorical socks off!

The speaker lineup:

- Falah Shazib - How Not To Build A Virtual Escape Room
- Anubhav Srivastava - The Fast Inverse Square Root Algorithm
- Frieda Rong - Arrow's Impossibility Theorem
- Charlie Wang - Gentle Introduction to Meltdown
- Stacy Gaikovaia - Intro to Verilog: Hardware Simulation and Synthesis
- William Gertler - Ethics in the Age of Lonely Qubits

<!-- -->

Food and Bubble Tea will be provided!

