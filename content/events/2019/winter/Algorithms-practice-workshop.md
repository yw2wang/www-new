---
name: 'Algorithms practice workshop'
short: 'Algorithms practice workshop on January 24th, 2019, at 6:30 PM in STC 0060'
startDate: 'January 24 2019 18:30'
online: false
location: 'STC 0060'
---

Algorithms practice workshop on January 24th, 2019, at 6:30 PM in STC 0060

Worried about your upcoming WaterlooWorks technical interviews? Want to nail your dream internship this term? Come to the Algorithms Practice Workshop at 6:30 PM, January 24th at STC 0060!

During the workshop, you will learn tons of useful technical interview tips collected from experienced upper-year students, as well as gain hands-on experience by practicing efficiently!

Writing a good solution to the problem is only the beginning — technical interviewers are evaluating more than just your code. Let's learn how to impress them in multiple ways!

Please register [here](<https://goo.gl/forms/WnUOd5VWwUfwA3Ru2>)


[Facebook event page](<https://www.facebook.com/events/554508501696081>)

