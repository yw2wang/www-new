---
name: 'Code Party 0'
short: 'Code Party 0 on March 13th, 2019, at 6 PM in QNC 1502'
startDate: 'March 13 2019 18:00'
online: false
location: 'QNC 1502'
---

Code Party 0 on March 13th, 2019, at 6 PM in QNC 1502

The CS Club is hosting our first Code Party of the term from 6 pm until 9 pm in QNC-1502, on Wednesday, March 13. Come code with us, eat some food, do some things.

Personal projects you want to work on? Homework projects you need to finish? Or want some time to explore some new technology and chat about it? You can join us at Code Party 0 and do it, with great company and great food. Come any time after 6 pm.

