---
name: 'Git 101'
short: 'Git 101 workshop on March 2nd, 2019, at 12 PM in QNC 1502'
startDate: 'March 02 2019 12:00'
online: false
location: 'QNC 1502'
---

Git 101 workshop on March 2nd, 2019, at 12 PM in QNC 1502

Git and source code management is an essential tool in software development. Knowing how to make the best out of it will help you get out of tricky situations, and allow you to be a better engineer.

Come learn some basic topics with us including the motivation for using git, interaction with git using the command line, pull requests, feature branch workflow, etc. This event targets first years with minimal git experience.

We are also searching for mentors for the workshop. Instructions for signing up are [here](<https://github.com/uwcsc/git-101-mentor-signup>)

Pizza will be provided.

