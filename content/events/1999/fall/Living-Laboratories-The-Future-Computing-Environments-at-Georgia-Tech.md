---
name: 'Living Laboratories: The Future Computing Environments at
  Georgia Tech'
short: 'By Blair MacIntyre and Elizabeth Mynatt'
startDate: 'October 18 1999 14:30'
online: false
location: 'DC1304'
---

by Blair MacIntyre and Elizabeth Mynatt

The Future Computing Environments (FCE) Group at Georgia Tech	is a collection of faculty and students that share a desire to	understand the partnership between humans and technology that	arises as computation and sensing become ubiquitous. With	expertise covering the breadth of Computer Science, but	focusing on HCI, Computational Perception, and Machine	Learning, the individual research agendas of the FCE faculty	are grounded in a number of shared "living laboratories" where	their research is applied to everyday life in the classroom	(Classroom 2000), the home (the Aware Home), the office	(Augmented Offices), and on one's person. Professors	MacIntyre and Mynatt will discuss a variety of these projects,	with an emphasis on the HCI and Computer Science aspects of	the FCE work.

In addition to their affiliation with the FCE group,	Professors Mynatt and MacIntyre are both members of the	Graphics, Visualization and Usability Center (GVU) at Georgia	Tech. This interdisciplinary center brings together research	in computer science, psychology, industrial engineering,	architecture and media design by examining the role of	computation in our everyday lives. During the talk, they will	touch on some of the research and educational opportunities	available at both GVU and the College of Computing.

