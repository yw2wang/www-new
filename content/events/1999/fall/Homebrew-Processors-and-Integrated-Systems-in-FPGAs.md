---
name: 'Homebrew Processors and Integrated Systems in FPGAs'
short: 'By Jan Gray'
startDate: 'December 01 1999 16:30'
online: false
location: 'MC2066'
---

by Jan Gray

With the advent of large inexpensive field-programmable gate arrays and tools it is now practical for anyone to design and build custom processors and systems-on-a-chip. Jan will discuss designing with FPGAs, and present the design and implementation of xr16, yet another FPGA-based RISC computer system with integrated peripherals.

Jan is a past CSC pres., B.Math. CS/EEE '87, and wrote compilers, tools, and middleware at Microsoft from 1987-1998. He built the first 32-bit FPGA CPU and system-on-a-chip in 1995.

