---
name: 'S02
    elections'
short: 'Come and vote for this term''s exec'
startDate: 'May 11 2002 19:00'
online: false
location: 'MC3036'
---

Vote for the exec this term. Meet at the CSC office.

