---
name: 'The GNU General Public License'
short: 'The teeth of Free Software'
startDate: 'November 07 2002 17:30'
online: false
location: 'MC4063'
---



> * The licenses for most software are designed to take away your freedom to share and change it. By contrast, the GNU General Public License is intended to guarantee your freedom to share and change free software---to make sure the software is free for all its users. *

> 
> \--- Excerpt from the GNU GPL

The GNU General Public License is one of the most influential software licenses in this day. Written by Richard Stallman for the GNU Project, it is used by software developers around the world to protect their work.

Unfortunately, software developers do not read licenses thoroughly, nor well. In this talk, we will read the entire GNU GPL and explain the implications of its passages. Along the way, we will debunk some myths and clarify common misunderstandings.

After this session, you ought to understand what the GNU GPL means, how to use it, and when you cannot use it. This session should also give you some insight into the social implications of this work.

