---
name: 'Automatic Memory Management and Garbage Collection'
short: 'A talk by James A. Morrison'
startDate: 'November 12 2002 16:30'
online: false
location: 'MC4058'
---

Do you ever wonder what java is doing while you wait? Have you ever used Modula-3? Do you wonder how lazily you can Mark and Sweep? Would you like to know how to Stop-and-Copy?

Come out to this talk and learn these things and more. No prior knowledge of Garbage Collection or memory management is needed.

