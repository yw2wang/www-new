---
name: 'Pints with the Profs'
short: 'Get to know your profs and be the envy of your friends!'
startDate: 'October 01 2002 18:30'
online: false
location: 'The Bomber'
---

Come out and meet your professors. This is a great opportunity to meet professors for Undergraduate Research jobs or to find out who you might have for future courses.

Profs who have confirmed their attendance are:

- Troy Vasiga, School of Computer Science
- J.P. Pretti, St. Jerome's and School of Computer Science
- Michael McCool, School of Computer Science, CGL
- Martin Karsten, School of Computer Science, BBCR
- Gisli Hjaltason, School of Computer Science, DB

<!-- -->

There will also be...

- Free Food
- Free Food
- Free Food

<!-- -->

