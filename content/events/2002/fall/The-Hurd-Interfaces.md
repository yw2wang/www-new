---
name: 'The Hurd Interfaces'
short: 'Marcus Brinkmann, a GNU Hurd developer, talks about the Hurd server interfaces, at the heart of a GNU/Hurd system'
startDate: 'October 26 2002 15:00'
online: false
location: 'MC2066'
---

The Hurd server interfaces are at the heart of the Hurd system. They define the remote procedure calls (RPCs) that are used by the servers, the GNU C library and the utility programs to communicate with the Hurd system and to implement the POSIX personality of the Hurd as well as other features.

This talk is a walk through the Hurd RPCs, and will give an overview of how they are used to implement the system. Individual RPCs will be used to illustrate important or exciting features of the Hurd system in general, and it will be shown how those features are accessible to the user at the command line, too.

---

Marcus Brinkmann is a math student at the Ruhr-Universitaet Bochum in Germany. He is one of maintainers of the GNU Hurd project and the initiator of the Debian GNU/Hurd binary distribution. He designed and implemented the console subsystem of the Hurd, wrote the FAT filesystem server, and fixed a lot of bugs, thus increasing the stability and usability of the system.

