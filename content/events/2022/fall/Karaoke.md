---
name: 'CSC Karaoke'
short: 'CSC Karaoke is coming right up! This is the perfect opportunity to sing and gather around with friends to have fun!'
startDate: 'October 25 2022 19:00'
endDate: 'October 25 2022 21:00'
online: false
location: 'DC 1351'
poster: 'images/events/2022/fall/Karaoke.png'
registerLink: 'https://docs.google.com/forms/d/e/1FAIpQLSfKLHY_Yv3c3OFTNzGuss0WvF8PGDhWypbaRO49oPCdlGBvNA/viewform'
---

🎶 CSC Karaoke is coming right up! This is the perfect opportunity to sing and gather around with friends to have fun!

📍 The date is Tuesday, October 25th, from 7-9 PM in DC 1351. Make sure to come and express your creativity through singing! 🎤
