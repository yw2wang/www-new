---
name: 'Code Party'
short: 'Come to our first code party of the term! You can chill out, work on side-projects 💻, or finish up on homework and assignments 📚!'
startDate: 'October 5 2022 18:00'
endDate: 'October 5 2022 20:00'
online: false
location: 'STC 0060'
poster: 'images/events/2022/fall/Code-Party-1.png'
---

📣 📣 Come to our first code party of the term! You can chill out, work on side-projects 💻, or finish up on homework and assignments 📚. There will also be free pizza 🍕 while you are working away or playing board games 🎲 with a fellow CSC friend.

🗓️ Event date: October 5th from 6 pm - 8 pm

📌 Location: STC 0060

Registration is not required.

We hope to see you there! 
