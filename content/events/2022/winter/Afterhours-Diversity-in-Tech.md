---
name: 'Afterhours: Diversity in Tech'
short: 'This Afterhours will be a discussion about why there is still a big difference in the percentage of POC and women in the tech workforce, and what we can do to combat such an issue.'
startDate: 'January 29 2022 18:00'
online: true
location: 'Zoom'
poster: 'images/events/2022/winter/Afterhours-Diversity-in-Tech.png'
registerLink: https://forms.gle/dH2FgruFkgXNkXeR8
---

CSC's Afterhours is back for Winter 2022! Afterhours is a series where we get the opportunity to dive into uncomfortable topics and share our experiences, as well as tips and tricks on how to overcome these problems.

Although more tech companies have been looking to improve upon the issue of lack of diversity, there is still a big difference in the percentage of POC and women in the tech workforce. 👩‍💻 This event will be a discussion about why such a gap exists and a discussion as to what we can do to combat such an issue. Speakers will share their personal stories, and the conversation will be open for all participants to discuss and share their own experiences and ideas. 💬

Registration is **required** for attendance. We'll be sending you a calendar invite, as well as an email reminder on the day of the event.

📅 Event Date: January 29th, 6:00 - 7:00pm ET via Zoom 💻

👉 Register at https://forms.gle/dH2FgruFkgXNkXeR8! Alternatively, you can also email us at exec@csclub.uwaterloo.ca to sign up as well.
