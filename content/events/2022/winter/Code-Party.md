---
name: "Code Party"
short: "CS Club is hosting our first Code Party since the pandemic started. Come code and chill (in-person!) with us!"
startDate: "March 25 2022 18:00"
online: false
location: "STC 0060"
poster: "images/events/2022/winter/Code-Party.png"
registerLink: "https://forms.gle/M4YzJeHpt2RiY1HHA"
---

CS Club is hosting our first Code Party since the pandemic started, from 6 pm until 9 pm in STC 0060, on Friday, March 25. Come code and chill (in-person!) with us!

Personal projects you want to work on? Homework assignments you need to finish? Or want some time to explore new technology and meet new people in a casual, minimal-stress environment? You can do all this and more at our Code Party! All experience levels are welcome.

Come any time after 6 pm. Food will be provided.

Registration is not required, but it is highly recommended. Register at https://forms.gle/M4YzJeHpt2RiY1HHA. Alternatively, you can also email us at exec@csclub.uwaterloo.ca to let us know you're interested.
