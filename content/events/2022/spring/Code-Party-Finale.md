---
name: 'Code Party: The Finale'
short: 'Come to our last Code Party of the term, to take a break between exams or to celebrate finishing them!'
startDate: 'August 11 2022 19:00'
endDate: 'August 11 2022 21:00'
online: false
location: 'STC 0060'
---
📣 The LAST Code Party of the term is on Thursday! 📣

🤩 Come out to enjoy a break from exams or to celebrate finishing them!

📅 Event date: August 11th, 7-9PM at STC 0060
