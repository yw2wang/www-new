---
name: 'Code Party; The Sequel'
short: 'CSC Code Party is Back!'
startDate: 'July 14 2022 19:00'
endDate: 'July 14 2022 21:00'
online: false
location: 'STC 0060'
poster: 'images/events/2022/spring/Code-Party-Sequel.png'
registerLink: 'https://forms.gle/txwwN6okM1FvYQtx5'
---
📣 CSC Code Party is Back!

💻 Are you looking for a chance to practice coding with friends and discover where your skills can improve? Come to Code Party: The Sequel to enjoy a fun coding session with your peers! Feel free to bring your projects and assignments to work on. 

📅  This event is being held on July 14 7PM-9PM at STC 0060. 

Sign up from this link: https://forms.gle/txwwN6okM1FvYQtx5
