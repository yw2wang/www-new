---
name: 'Afterhours'
short: "If you want to talk about topics overlooked in the daily grind of university and co-op, come join us for CSC Afterhours."
startDate: 'July 19 2022 19:00'
endDate: 'July 19 2022 20:30'
online: false
location: 'SLC Multipurpose Room'
poster: 'images/events/2022/spring/Afterhours.png'
registerLink: https://forms.gle/qhEXgKrhL5XcdD4b7
---
📣 If you want to talk about topics overlooked in the daily grind of university and co-op, come join us for CSC Afterhours.

🤩 Afterhours is a space for CSC members to discuss topics ranging from happiness😃, finding ways to grow outside of uni/career🏫, becoming an adult, managing relationships, and avoiding academic/job search toxicity, and many more.

✨ If any of these topics have been weighing on your mind recently, we encourage you to come out, hear the stories of others, discuss your own thoughts, and feel a little less alone.

📌 Feel free to rotate between discussions in a close-knit, non-judgmental environment. 
 
🗓 Date: July 19th, 7:00-8:30PM at SLC Multipurpose room. 

👉 Sign up from this link: https://forms.gle/qhEXgKrhL5XcdD4b7