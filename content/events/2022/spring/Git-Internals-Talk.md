---
name: "Git Internals Talk"
short: "Have you heard of Git or used it in your past coop terms? Come join us for a deep dive under the hood!"
startDate: "July 25 2022 18:00"
endDate: "July 25 2022 19:00"
online: false
location: "MC 5479"
poster: "images/events/2022/spring/Git-Internals-Talk.png"
---

🚀 Have you heard of Git or used it in your past coop terms?

🖥️ Most software development these days is done using git. Git can seem very complex to use (and often is!). However, it turns out the core architecture behind git is quite simple and elegant.

🤔 We will explore how git works under the hood to understand this architecture, and demystify what commonly used git commands actually do. Afterwards, we will dive deeper into the implications of this architecture, both positive and negative, as well as how this knowledge can be useful day to day. We will end it off by answering the question: “Is git the first blockchain?” (spoiler: sort of!) There are drinks/snacks served at the event!

🗓 Event Date: July 25th, at MC 5479 from 6-7 PM

👉 Sign up is not required for this event!
