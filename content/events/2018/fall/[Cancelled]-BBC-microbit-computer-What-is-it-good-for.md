---
name: '[Cancelled] BBC micro:bit computer: What is it good for?'
short: 'Professor Richard Mann will be talking about the BBC micro:bit, an embedded computer that is popular with hobbyists and comes with a variety of peripherals.'
startDate: 'November 05 2018 17:30'
online: false
location: 'MC-4063'
---

This talk was cancelled. The material for the talk can be found [here.](<http://www.cs.uwaterloo.ca/~mannr>)

### Abstract:

BBC micro:bit (microbit.org) was introduced in 2015 and has since become popular with educators and hobbyists.

Micro:bit uses an ARM Cortex M0 processor running the "mbed" OS/runtime (mbed.arm.com). It has a built in LED 7x7 array, two buttons, compass, accelerometer, infra red transceivers, and low power wireless communication. Most importantly, it has multiple analog and digital pins to connect to the external world.

Web based tools compile gui/blocks, javascript, or python to executable (HEX) files that run on the device. The device appears as a USB drive. It is programmed by copying (dragging) the HEX image to the device. Once programmed, the device runs standalone and communicates with the the host computer via a serial port API.

All of this is great fun and a gateway into electronics and real time programming.

In this talk I will present a brief introduction to micro:bit, electronics, and electronic signal measurement (voltmeter, function generator, oscilloscope).

We will measure the run time performance of the micro:bit, in particular the operation of the analog inputs and outputs and the response time/latency of the device and consider its suitability for user interface, music and audio projects.

### Bio:

Richard Mann is Associate Professor in Computer Science. His research is in AI, Sound/Audio, Acoustics, and Electro/acoustic measurement. Details at www.cs.uwaterloo.ca/\~mannr

In W19 I will teach: CS489 -- Advanced topics, Computational Sound and Audio. This is a project-based course (no final).

I am also looking for URA students in the Sound/Audio area.

