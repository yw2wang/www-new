---
name: 'Winter 2018 Elections'
short: 'The Computer Science Club will be holding elections for the Winter 2018 President, Vice-President, Secretary and Treasurer. Additionally, the Systems Administrator, Office Manager and Librarian, CTF Club Liaison and IMAPd (fridge and snack runs) will be appointed.'
startDate: 'January 15 2018 17:00'
online: false
location: 'MC Comfy'
---

The Computer Science Club will be holding elections for the Winter 2018 term on Monday, January 15th at 5:00pm in the MC Comfy Lounge (MC 3001). There will be snacks at the elections, probably Timbits.

The following positions will be elected: President, Vice-President, Treasurer and Secretary. The following positions will be appointed: Systems Administrator (to be ratified at the meeting), Office Manager, Librarian and IMAPd (fridge and snack runs).

If you would like to run or nominate someone for any of the elected positions, you can put your/their name and Quest ID as well as a list of positions on a piece of paper into the nominations box in the CSC office (MC 3036/3037) or send an email to the Chief Returning Officer at [cro@csclub.uwaterloo.ca](<mailto:cro@csclub.uwaterloo.ca>).

I will periodically empty the nominations box and notify the people nominated via their @csclub.uwaterloo.ca (or failing that, @edu.uwaterloo.ca) email address. Please note that club officer positions (elected positions, plus Systems Administrator) are restricted to MathSoc social members.

Nominations will close at 5:00pm on Sunday, January 14th (24 hours prior to the start of elections). At this time, I will publish the list of nominations via the CSC mailing list as well as at [ https://csclub.uwaterloo.ca/elections ](<https://csclub.uwaterloo.ca/elections>).

Voting will be done in a heads-down, hands-up manner and is restricted to MathSoc social members. A full description of the roles and the election procedure are listed in our Constitution, available at [ https://csclub.uwaterloo.ca/about/constitution. ](<https://csclub.uwaterloo.ca/about/constitution>)

Any questions related to the election can be directed to [cro@csclub.uwaterloo.ca](<mailto:cro@csclub.uwaterloo.ca>).

