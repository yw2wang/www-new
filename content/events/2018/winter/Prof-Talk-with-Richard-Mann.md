---
name: 'Prof Talk with Richard Mann'
short: 'Professor Richard Mann will be giving a talk on black-box testing of audio gear. Come out to see fancy audio gear, learn more about his Advanced Topics course in Computational Sound, and eat free food! Click through for his abstract.'
startDate: 'March 22 2018 18:00'
online: false
location: 'MC 4059'
---

Black box testing usually refers to computer testing, either software or hardware.

In this talk I apply similar ideas to testing analog and digital audio gear. For example, given an audio device, such as a guitar effects pedal, can we stimulate the system with test signals and determine what processing is done inside?

I will present our open source testing software to test the frequency response, time delay and distortion in audio systems. We will show several real world testing situations, including microphones, loud speakers, digital keyboards, digital audio mixing boards, and guitar effects pedals.

Students are encouraged to bring their own musical instruments and/or sound processors for testing.

Finally, I will present information about my current audio course, CS489/689 -- Advanced Topics in Computer Science -- Computational Sound. This is a project based course, normally offered in the Winter term.

