---
name: 'Alt-Tab: W18'
short: ' CSC will be hosting our termly Alt-Tab event, the Computer Science version of Short Attention Span Math Seminars (SASMS) hosted by that math club down the hall. It will be a night full of friendly talks. '
startDate: 'March 28 2018 18:00'
online: false
location: 'MC 4020'
---

Our current lineup includes: 
 
- Ifaz Kabir: Efficient Type Inference with Union Find
- Jordan Garside: GraphQL and APIs
- Sean Harrap: Implementing Structs Almost From Scratch
- Ashish Gaurav: Teaching Programs to play Simple Games
- Jennifer Zhou: Garbage Collection Concepts
- Lessons Learned from Cross-Compiling Rust
<!-- -->

