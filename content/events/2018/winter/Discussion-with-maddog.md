---
name: 'Discussion with maddog'
short: 'We''ll be having a discussion session with maddog, an out-of-town speaker from the LPI. Food will be provided, as well as good company. Come out!'
startDate: 'March 13 2018 18:00'
online: false
location: 'MC Comfy'
---

Jon "maddog" Hall is the Executive Director of Linux International, an association of computer users who wish to support and promote the Linux Operating System. During his career in commercial computing which started in 1969 (almost a half-century ago), Mr. Hall has been a programmer, systems designer, systems administrator, product manager, technical marketing manager, educator, and consultant. 


 He has worked for such companies as Western Electric Corporation, Aetna Life and Casualty, Bell Laboratories, Digital Equipment Corporation, VA Linux Systems, SGI and Futura Networks (Campus Party). 


 Mr. Hall is currently the CEO of OptDyn, Inc (www.optdyn.com) which creates the Subutai(tm) suite of Open Source Peer-to-Peer Cloud computing tools. He also works as an independent consultant, and is involved with bringing environmentally friendly computing to emerging marketplaces, as well as working on performance and educational issues with Free and Open Source Software via the Linaro Association. He is the Chairman Emeritus of wit.com 


 Mr Hall has worked on many systems, both proprietary and open, having concentrated on Unix systems since 1980 and Linux systems since 1994 (almost a quarter century ago), when he first met Linus Torvalds and correctly recognized the commercial importance of Linux and Free and Open Source Software. 


 He has taught at Hartford State Technical College, Merrimack College and Daniel Webster College. He still likes talking to students over pizza and beer (the pizza can be optional). 


 Mr. Hall is the author of numerous magazine and newspaper articles, many presentations and one book, "Linux for Dummies". He currently writes a monthly article for Linux Pro Magazine and occasionally blogs for them on their web site. 


 Mr. Hall has consulted with the governments of China, Malaysia and Brazil as well as the United Nations and many local and state governments on the use of Free and Open Source Software. 


 Mr. Hall has served and serves on the boards of several companies, universities and several non-profit organizations. He is currently very active with the University of Sao Paulo's Centro Interdisciplinar Em Tecnologias Interativas (CITI), acting as a member of their advisory board. Mr. Hall is also the Board Chair of the Linux Professional Institute, the world's premier Open Source Certification organization, and is the senior advisor and co-founder of Caninos Loucos, bringing inexpensive, locally designed and manufactured single board computers to Brazil. He is also the President of Project Caua. 


 Mr. Hall has traveled the world (over 100 countries) speaking on the benefits of Free and Open Source Software, and received his BS in Commerce and Engineering from Drexel University (1973), and his MSCS from RPI in Troy, New York (1977).

