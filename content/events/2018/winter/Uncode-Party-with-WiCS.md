---
name: 'Uncode Party with WiCS'
short: ' We are having a joint Code Party with Women in Computer Science (WiCS). This time, it''s an Uncode Party, where you try to find the worst solutions possible to programming problems that we will provide. '
startDate: 'March 26 2018 19:00'
online: false
location: 'QNC 2502'
---

An example of a good "bad" solution is [SlowSort](<http://wiki.c2.com/?SlowSort>). Come and write shoddy code with us and eat free food. You can work on your assignments too. No general meeting will be bundled with this event.

