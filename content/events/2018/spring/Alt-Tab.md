---
name: 'Alt-Tab'
short: 'Come hear 10-15 minutes talks from CSC members on a variety of topics.'
startDate: 'July 04 2018 18:00'
online: false
location: 'MC 4058'
---

Come hear 10-15 minutes talks from CSC members on a variety of topics. Currently planned talks include:

- Abstract Machines, what a PL person thinks a computer looks like!
- Rendering with Signed Distance Fields
- Strength, weaknesses, and applications of genetic algorithms

<!-- -->

Interested in talking? Email djauhar@edu.uwaterloo.ca with your title and a short abstract.

