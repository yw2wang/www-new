---
name: 'Code Party and Contest Finale'
short: 'Come on out for a night of code, contests, and energy drinks. Join the Computer Scinece Club for the finale of the Google AI Challenge and an all night code party. Finish up your entry, or start it (its not too late). Not interested in the contest? Come out anyway for a night of coding and comradarie with us.'
startDate: 'October 16 2009 19:00'
online: false
location: 'Comfy Lounge'
---

Come on out for a night of code, contests, and energy drinks. Join the Computer Scinece Club for the finale of the Google AI Challenge and an all night code party. Finish up your entry, or start it (its not too late). Not interested in the contest? Come out anyway for a night of coding and comradarie with us.

Included in the party will be the contest finale and awards cerimony, so if you've entered be sure to stick arround to collect the spoils of victory, or see just who that person you couldn't edge off is.

