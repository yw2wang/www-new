---
name: 'The Club That Really Likes Dinner'
short: 'Come on out to the club''s termly end of term dinner, details in the abstract'
startDate: 'December 05 2009 18:30'
online: false
location: 'MC3036'
---

The dinner will be potluck style at the Vice President's house, please RSVP (respond swiftly to the vice president) [here](<https://csclub.uwaterloo.ca/rsvp>) if you plan on attending. If you don't know how to get there meet at the club office at 6:30 PM, a group will be leaving to lead you there.

