---
name: 'PHP on Windows'
short: 'PHP Programming Contest Info Session'
startDate: 'May 12 2009 12:00'
online: false
location: 'MC 2034'
---

Port or create a new PHP web application and you could win a prize of up to $10k. Microsoft is running a programming contest for PHP developers willing to support the Windows platform. The contest is ongoing; this will be a short introduction to it by representatives of Microsoft and an opportunity to ask questions. Pizza and pop will be provided.

