---
name: 'Paper Club'
short: 'Come and drink tea and read an academic CS paper with the Paper Club. We will be meeting from 4:30pm until 6:30pm on Monday, June 22th on the 4th floor of the MC (exact room number TBA). See http://csclub.uwaterloo.ca/\~paper'
startDate: 'June 22 2009 16:30'
online: false
location: 'MC 4041'
---

Come and drink tea and read an academic CS paper with the Paper Club. We will be meeting from 4:30pm until 6:30pm on Monday, June 22th on the 4th floor of the MC (exact room number TBA). See http://csclub.uwaterloo.ca/\~paper

