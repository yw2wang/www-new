---
name: 'Dooly''s Night'
short: 'Come join the CSC as we head to Dooly''s.'
startDate: 'February 27 2009 17:00'
online: false
location: 'CSC Office: MC3036'
---

Meet us at the Club office as we head to Dooly's for cheap tables and good times.

