---
name: 'Roasting Our First Year Resumes'
short: 'Upper years students will be critiquing their own resumes from first year.👉 Streamed at [twitch.tv/uwcsclub](<https://twitch.tv/uwcsclub>)'
startDate: 'November 25 2020 19:00'
online: true
location: 'Online'
---

Looking to start your resume for next term but don’t know where to start? CSC is hosting a Roasting Our First Year Resumes event.

Join us on November 25 from 7-8pm on Twitch to hear from some Waterloo upper years as they critique their resumes from first year.

Be sure to tune in to hear resume tips from a diverse group of upper years in varying tech domains and bring your questions for the Q&A!

The event will be streamed at [twitch.tv/uwcsclub](<https://twitch.tv/uwcsclub>)

