---
name: 'CSC Circles Easter Egg Hunt'
short: 'Come join us for the last Circles event of the term, featuring an easter egg hunt, and snacks and board games!'
startDate: 'March 27 2024 19:00'
endDate: 'March 27 2024 21:00'
online: false
location: 'TBD'
poster: 'images/events/2024/winter/1711581816387--CSC-Circles-Easter-Egg.png'
registerLink: 'https://forms.gle/kPzGVqraySYAVKQ48'
---

📢 Come celebrate Easter with CSC for our last Circles event, an Easter egg hunt (there are a lot of chocolate eggs to be found 👀)

🐰 Whether or not you attended the previous events, come sign up for this event individually (we’ll match you with a group) or as a group of 4-5 to hunt for chocolate eggs!

💫 There will also be board games and snacks available for everyone!

📝 Sign up by March 25th, 11:59PM using the following link: https://forms.gle/kPzGVqraySYAVKQ48
