---
name: 'Rust Workshop with Patrik Buhring'
short: "Come join us for a series of workshops following the 'Google Comprehensive Rust' course aimed to get you fully productive in Rust."
startDate: 'February 03 2024 14:00'
endDate: 'March 10 2024 17:00'
online: false
location: 'MC 1065'
poster: 'images/events/2024/winter/1707085016729--Rust-Workshop.jpg'
---

🚀  This workshop should get you up and running in Rust from completely productive. We'll be covering all the main features of the language, going from 'Hello, World' to poking around in unsafe Rust.

🦀 The workshop will be run by a 6-years-practicing Rustacean, so no questions are off the table!

📍 Location: MC 1065

📅 Workshop Days: Saturdays and Sundays February 3rd & 4th, 10th & 11th, March 2nd & 3rd, 9th & 10th

⏰ Time: 2 - 5 PM

🎥 Please join the Discord server: https://discord.gg/q3pdTg46xN for further communication. It'll be streamed online too if you can't make it in person!
