---
name: 'UW''s CS curriculum: past, present, and future'
short: 'Come out to here Prabhakar Ragde talk about our UW''s CS curriculum'
startDate: 'June 07 2005 16:00'
online: false
location: 'MC 4042'
---

I'll survey the evolution of our computer science curriculum over the past thirty-five years to try to convey the reasons (not always entirely rational) behind our current mix of courses and their division into core and optional. After some remarks about constraints and opportunities in the near future, I'll open the floor to discussion, and hope to hear some candid comments about the state of CS at UW and how it might be improved.

About the speaker:

Prabhakar Ragde is a Professor in the School of Computer Science at UW. He was Associate Chair for Curricula during the period that saw the creation of the Bioinformatics and Software Engineering programs, the creation of the BCS degree, and the strengthening of the BMath/CS degree.

