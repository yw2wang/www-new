---
name: 'UNIX 101'
short: 'First UNIX tutorial'
startDate: 'October 04 2005 16:30'
online: false
location: 'MC 2037'
---

The CSC UNIX tutorials are intended to help first year CS and other interested learn UNIX and the CS UNIX environment.

This is the first in a series of two or three tutorials. It will cover basic shell use, and simple text editors.

