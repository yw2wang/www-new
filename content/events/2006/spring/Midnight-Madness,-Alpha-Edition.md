---
name: 'Midnight Madness, Alpha Edition'
short: 'Come out to discuss current & future plans/projects for the Club'
startDate: 'July 17 2006 23:59'
online: false
location: 'MC3036'
---

The Computer Science Club (CSClub) has "new" DEC Alphas which are most awesome. Come out, help take them part, put them back together, solder, and eat free food (probably pizza).

