---
name: 'CSC
Elections'
short: 'Come out and vote for the Spring 2006 executive!'
startDate: 'May 08 2006 16:30'
online: false
location: 'The Comfy Lounge'
---

The Computer Science Club will be holding its elections for the Spring 2006 term on Monday, May 8th. The elections will be held at 4:30 PM in the Comfy Lounge, on the 3rd floor of the MC. Please remember to come out and vote!

We are accepting nominations for the following positions: President, Vice-President, Treasurer, and Secretary. The nomination period continues until 4:30 PM on Sunday, May 7th. If you are interested in running for a position, or would like to nominate someone else, please email cro@csclub.uwaterloo.ca before the deadline.

