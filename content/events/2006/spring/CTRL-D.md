---
name: 'CTRL D'
short: 'Come out for the Club that Really Likes Dinner'
startDate: 'July 29 2006 19:00'
online: false
location: 'East Side Mario'
---

Summer: the sparrows whistle through the teapot-steam breeze. The ubiquitous construction team tears the same pavement up for the third time, hammering passers-by with dust and noise: our shirts, worn for the third time, noisome from competing heat and shame. As Nature continues her Keynesian rotation of policy, and as society decrees yet another parting of ways, it is proper for the common victims to have an evening to themselves, looking both back and ahead, imagining new opportunities, and recognising those long since missed. God fucking damn it.

This term's CTRL-D end-of-term dinner is taking place tomorrow (Saturday) at 7:00 P.M. at East Side Mario's, in the plaza. Meet in the C.S.C. fifteen minutes beforehand, so they don't take away our seats or anything nasty like that.

A lot of people wanted to go to the Mongolian Grill, but I'm pretty sure this place has a similar price-to-tasty ratio; what's more, they'll actually grant us a reservation more than four nights a week. I've confirmed that the crazy allergenic peanuts no longer exist (sad), and they have a good vegetarian selection, which is likely coincides with their kosher and halal menus.

Come out for the tasty and the awesome! If you pretend it's your birthday, everyone's a loser! Tell your friends, because I told the telephone I wanted to reserve for 10 to 12 people, and I don't wish to sully Calum T. Dalek's good name!

