---
name: 'Simulating multi-tasking on an embedded architecture'
short: 'Alex Tsay will look at the common hack used to simulate multi-processing in a real time embedded environment.'
startDate: 'July 20 2006 16:30'
online: false
location: 'MC4041'
---

In an embedded environment resources are fairly limited, especially. Typically an embedded system has strict time constraints in which it must respond to hardware driven interrupts and do some processing of its own. A full fledged OS would consume most of the available resources, hence crazy hacks must be used to get the benefits without paying the high costs. This talk will look at the common hack used to simulate multi-processing in a real time embedded environment.

