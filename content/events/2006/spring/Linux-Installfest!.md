---
name: 'Linux Installfest!'
short: 'A part of Linux Awareness Week'
startDate: 'July 25 2006 13:00'
online: false
location: 'DC Fishbowk'
---

The Computer Science Club is once again stepping forward to fulfill its ancient duty to the people-this time by installing one of the many fine distributions of Linux for you.

Ubuntu? Debian? Gentoo? Fedora? We might not have them all, but we seem to have an awful lot! Bring your boxen down to the D.C. Fishbowl for the awesome!

Install Linux on your machine-install fear in your opponents!

