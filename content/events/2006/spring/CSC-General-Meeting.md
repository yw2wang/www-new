---
name: 'CSC General Meeting'
short: 'Come out to discuss current & future plans/projects for the Club'
startDate: 'June 21 2006 16:30'
online: false
location: 'MC4042'
---

The venue will include:



- Computer usage agreement discussion (Holden has some changes he'd like to propose)

- Web site - Juti is redesigning the web site (you can see [a beta here](<beta/>) \- ideas are welcome.

- Frosh Linux cd's that could be put in frosh math faculty kits.

- VoIP "not phone services" ideas.

- Ideas for talks (people, topics, etc...). We requested Steve Jobs and Steve Balmer, so no idea is too crazy.

- Ideas for books.

- General improvements/comments for the club.


<!-- -->

If you have ideas, but can't attend, please email them to [president@csclub.uwaterloo.ca](<mailto:president@csclub.uwaterloo.ca>) and they will be read them at the meeting.

