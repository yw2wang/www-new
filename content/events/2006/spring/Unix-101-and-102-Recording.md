---
name: 'Unix 101 and 102 Recording'
short: 'Unix 101 and 102 recording'
startDate: 'May 14 2006 13:00'
online: false
location: 'CSC'
---

Have you heard of our famous Unix 101 and Unix 102 tutorials. We've decided to try and put them on the web. This Sunday we will be doing a first take. At the same time, we're going to be looking at adding new material that we haven't covered in the past.

Why should you come out? Not only will you get to hang out with a wonderful group of people, you can help impart your knowledge to the world. Don't know anything about Unix? That's cool too, we need people to make sure its easy to follow along and hopefully keep us from leaving something out by mistake.

