---
name: 'March of the Penguins'
short: 'The Computer Science Club will be showing March of the Penguins'
startDate: 'July 21 2006 17:30'
online: false
location: 'MC1085'
---

[March of the Penguins](<http://wip.warnerbros.com/marchofthepenguins/>) , an epic nature documentary, as dictated by some guy with a funny voice is being shown by the Computer Science club because penguins are cute and were bored [that and the whole Linux awareness week that forgot to tell people about].

