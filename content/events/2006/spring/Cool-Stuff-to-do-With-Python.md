---
name: 'Cool Stuff to do With Python'
short: 'Albert O''Connor will be introducing the joys of programming in python'
startDate: 'July 20 2006 17:30'
online: false
location: 'MC4041'
---

Albert O'Connor, a UW grad, will be giving a \~30 minute talk on introducing the joys of programming python. Python is an open source object-oriented programming language which is most awesome.

