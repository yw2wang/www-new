---
name: 'Creating Killer Applications'
short: 'A talk by Larry Smith'
startDate: 'March 06 2006 16:45'
online: false
location: 'Physics 145'
---

A discussion of how software creators can identify application opportunities	that offer the promise of great social and commercial significance. Particular	attention will be paid to the challenge of acquiring cross domain knowledge	and setting up effective collaboration.

