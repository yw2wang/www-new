---
name: 'Pints With Profs'
short: 'Come out and meet your professors. Free food provided!'
startDate: 'February 09 2006 17:30'
online: false
location: 'Bombshelter Pub'
---

Come out and meet your professors! This is a great opportunity to mingle with your professors before midterms or find out who you might have for future courses. All are welcome!

Best of all, there will be **free food!**

You can pick up invitations for your professors at the Computer Science Club office in MC 3036.

Pints with Profs will be held this term on Thursday, 9 February 2006 from 5:30 to 8:00 PM in the Bombshelter.

