---
name: 'Hands On Seminar on Public Key Cryptography'
short: 'The fourth event in our security and privacy series. By undergraduate students Murphy Berzish and Nick Guenther.'
startDate: 'November 05 2013 18:00'
online: false
location: 'MC 3001 (Comfy)'
---

Nick Guenther and Murphy Berzish will be holding a hands-on seminar in the Comfy to introduce you to public-private key crypto and how you can practically use it, so bring your laptops! You will learn about PGP, an encryption protocol that provides confidentiality and authenticity. At the seminar, you will learn how to use PGP to send encrypted email and files, provably identify yourself to others, and verify data. Bring a laptop so we can help help you generate your first keypair and give you the chance to form a Web of Trust with your peers.

A GSIntroducer from [www.GSWoT.org](<www.GSWoT.org>) will be on hand. If you are interested in obtaining an elevated level of trust, bring government-issued photo-ID.

There will also be balloons and cake.

