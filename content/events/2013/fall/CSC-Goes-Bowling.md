---
name: 'CSC Goes Bowling'
short: 'All CSC members and their guests are invited for a night of free bowling at Bingemans! Transportation will be provided. If you are interested in attending, please RSVP using the online form by Oct. 18. You can find it by viewing this event in detail.'
startDate: 'October 30 2013 18:00'
online: false
location: 'Bingemans'
---

We are pleased to kick off the term with free bowling for all interested members at Bingemans! Transportation will be provided. If you are interested in attending, please RSVP using [this online form](<http://goo.gl/FsZIfK>) by Oct. 18.

Please note the event date change (Oct. 23 to Oct. 30). The bus will be leaving from the Davis Center at 6:00PM sharp on the 30th.

