---
name: 'Trust in ISPs'
short: 'This is the fifth lecture of six in the Security and Privacy Lecture Series. By founding member of the Canadian Cybersecurity Institute and employee of local ISP Sentex Sean Howard.'
startDate: 'November 12 2013 17:00'
online: false
location: 'MC 4060'
---

Bell's recent announcement of their use of Deep Packet Inspection (DPI) brings to light a long-standing issue: your internet service provider (ISP) pwns you. They control your IP allocation, your DNS, your ARP, the AS paths. The question has never been about ability—it's about trust. Whether Rogers, AT&T, Virgin, Telus, Vodafone or Wind, your onramp to the internet is your first and most potent point of security failure.

Founding member of the Canadian Cybersecurity Institute and employee of local ISP Sentex Sean Howard will vividly demo the reasons you need to be ble to trust your internet provider. Come for the talk, stay for the pizza!

This is the fifth lecture of six in the Security and Privacy Lecture Series.

