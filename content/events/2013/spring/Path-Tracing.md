---
name: 'Path Tracing'
short: 'As a follow on to last term''s tutorial on building a ray-tracer from scratch, this talk will be presenting the basic mechanics of how a bidirectional path-tracer creates a globally illuminated scene, advantages and limitations of this approach over other offline global illumination techniques along with a simple example path-tracer written in C++, and opportunities for hardware acceleration on GPUs, time permitting.'
startDate: 'July 18 2013 17:00'
online: false
location: 'MC 4041'
---

As a follow on to last term's tutorial on building a ray-tracer from scratch, this talk will be presenting the basic mechanics of how a bidirectional path-tracer creates a globally illuminated scene, advantages and limitations of this approach over other offline global illumination techniques along with a simple example path-tracer written in C++, and opportunities for hardware acceleration on GPUs, time permitting.

