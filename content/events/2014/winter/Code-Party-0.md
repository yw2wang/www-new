---
name: 'Code Party 0'
short: 'Immediately after UNIX 101, we will be having our first annual code party. Enjoy a free dinner, relax, and share ideas with your friends about your favourite topics in computer science.'
startDate: 'February 13 2014 18:30'
online: false
location: 'Comfy Lounge'
---

Immediately after UNIX 101, we will be having our first annual code party. Enjoy a free dinner, relax, and share ideas with your friends about your favourite topics in computer science.

