---
name: 'HackWaterloo'
short: 'Work on a software project for 24 hours in teams of up to 4 members. Swag will be provided by Facebook and Google. A Microsoft Surface Tablet will be awarded to the winning team. Register and find out more at [http://hack-waterloo.com](<http://hack-waterloo.com>).'
startDate: 'March 28 2014 19:00'
online: false
location: 'CPH 1346'
---

Work on a software project for 24 hours in teams of up to 4 members. Swag will be provided by Facebook and Google. A Microsoft Surface Tablet will be awarded to the winning team. Register and find out more at [http://hack-waterloo.com](<http://hack-waterloo.com>).

