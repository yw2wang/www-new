---
name: 'CSC Goes Outside...Again!'
short: 'Do you like going outside? Are you vitamin-D deficient from being in the MC too long? Do you think s''mores and bonfire are a delicious combination? If so, you should join us as the CSC is going outside again! Around 7:30PM, we''re going to Laurel Creek Fire Pit for some outdoor fun. Come throw frisbees, relax and eat snacks in good company - even if you aren''t a fan of the outside or vitamin-D deficient! We''ll also have some sort of real food - probably pizza.'
startDate: 'July 25 2014 19:30'
online: false
location: 'Laurel Creek Fire Pit'
---

Do you like going outside? Are you vitamin-D deficient from being in the MC too long? Do you think s'mores and bonfire are a delicious combination? If so, you should join us as the CSC is going outside again! Around 7:30PM, we're going to Laurel Creek Fire Pit for some outdoor fun. Come throw frisbees, relax and eat snacks in good company - even if you aren't a fan of the outside or vitamin-D deficient! We'll also have some sort of real food - probably pizza.

