---
name: 'Talk: Machine Learning at Bloomberg'
short: 'Kang, our guest speaker from Bloomberg, will illustrate some examples and difficulties associated with working on some of the most fascinating technical challenges in business and finance. He will also show some of the machine learning applications at Bloomberg that are useful in this environment. Please show up early to ensure a spot (and dinner).'
startDate: 'November 12 2014 17:30'
online: false
location: 'EIT 1015'
---

Kang, our guest speaker from Bloomberg, will illustrate some examples and difficulties associated with working on some of the most fascinating technical challenges in business and finance. He will also show some of the machine learning applications at Bloomberg that are useful in this environment. Please show up early to ensure a spot (and dinner).

