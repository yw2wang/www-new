---
name: 'Talk: Heroic Android HTTP'
short: 'The network is unreliable. 3G networking is slow. Using WiFi drains your battery. The NSA is spying on you. Different versions of HttpURLConnection have different bugs.Jesse Wilson, a software developer at Square, will be talking about OkHttp, a library that he maintains, and how to use it to make your app''s networking work even when conditions aren''t ideal. He will talk about how to configure caching to improve behavior and save resources. He will talk about crypto, and he will give advice on which libraries to use to make good networking easy.Please RSVP here: https://www.ticketfi.com/event/77/heroic-android-http.'
startDate: 'November 27 2014 18:00'
online: false
location: 'MC 4020'
---

The network is unreliable. 3G networking is slow. Using WiFi drains your battery. The NSA is spying on you. Different versions of HttpURLConnection have different bugs.

Jesse Wilson, a software developer at Square, will be talking about OkHttp, a library that he maintains, and how to use it to make your app's networking work even when conditions aren't ideal. He will talk about how to configure caching to improve behavior and save resources. He will talk about crypto, and he will give advice on which libraries to use to make good networking easy.

Please RSVP here: https://www.ticketfi.com/event/77/heroic-android-http.

