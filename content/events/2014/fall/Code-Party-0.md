---
name: 'Code Party 0'
short: 'Immediately after UNIX 101, we will be having our first annual code party. Enjoy a free dinner, relax, and share ideas with your friends about your favourite topics in computer science. Feel free to show up with or without personal projects to work on, we''ve got lots of ideas to get started with.'
startDate: 'October 24 2014 18:00'
online: false
location: 'MC Comfy'
---

Immediately after UNIX 101, we will be having our first annual code party. Enjoy a free dinner, relax, and share ideas with your friends about your favourite topics in computer science. Feel free to show up with or without personal projects to work on, we've got lots of ideas to get started with.

