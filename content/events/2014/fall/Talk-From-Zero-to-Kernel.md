---
name: 'Talk: From Zero to Kernel'
short: 'From the massive supercomputer, to your laptop, to a Raspberry Pi: all computing systems run on an operating system powered by a kernel. The kernel is the most fundamental software running on your computer, enabling developers and users to interact with its hardware at a higher level.This talk will explore the process of writing a minimal kernel from scratch, common kernel responsibilities, and explore of the challenges of kernel development.'
startDate: 'November 10 2014 05:30'
online: false
location: 'RCH 205'
---

From the massive supercomputer, to your laptop, to a Raspberry Pi: all computing systems run on an operating system powered by a kernel. The kernel is the most fundamental software running on your computer, enabling developers and users to interact with its hardware at a higher level.

This talk will explore the process of writing a minimal kernel from scratch, common kernel responsibilities, and explore of the challenges of kernel development.

