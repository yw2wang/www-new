---
name: 'Talk: Why Pattern Recognition is Hard, and Why Deep Neural Networks Help'
short: 'In the last few years, there has been breakthrough progress in pattern recognition -- problems like computer vision and voice recognition. This sudden progress has come from a powerful class of models called deep neural networks.This talk will explore what it means to do pattern recognition, why it is a hard problem, and why deep neural networks are so effective. We will also look at exciting and strange recent results, such as state of the art object recognition in images, neural nets playing video games, neural nets proving theorems, and neural nets learning to run python programs!Our speaker, Christopher Olah, is a math-obsessed and Haskell-loving research intern from Google''s Deep Learning group. He has a blog about his research here: http://colah.github.io/.'
startDate: 'November 17 2014 18:00'
online: false
location: 'QNC 1502'
---

In the last few years, there has been breakthrough progress in pattern recognition -- problems like computer vision and voice recognition. This sudden progress has come from a powerful class of models called deep neural networks.

This talk will explore what it means to do pattern recognition, why it is a hard problem, and why deep neural networks are so effective. We will also look at exciting and strange recent results, such as state of the art object recognition in images, neural nets playing video games, neural nets proving theorems, and neural nets learning to run python programs!

Our speaker, Christopher Olah, is a math-obsessed and Haskell-loving research intern from Google's Deep Learning group. He has a blog about his research here: http://colah.github.io/.

