---
name: 'Drop-in Resume Critiques (Day 1)'
short: 'Live Resume Critiques with Tech+, UW Data Science Club, and UW PM (registration required).'
startDate: 'May 15 2021 19:00'
online: true
location: 'Online'
---

Joining forces with Tech+, UW Data Science Club, and UW PM, we're hosting live resume critiques!

The session will be held on Discord; you must sign up at [http://bit.ly/S21-resume-critique](<http://bit.ly/S21-resume-critique>) to gain access to the critiques. Deadline to signup is Friday, May 14th at 11:59pm EDT.

