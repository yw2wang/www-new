---
name: 'Succeeding in First Year Academics'
short: 'Come join a panelist of upper years who can give insight on how to better succeed in first year academics.'
startDate: 'October 30 2021 14:00'
online: true
location: 'Zoom'
poster: 'images/events/2021/fall/succeeding-in-first-year.png'
registerLink: https://bit.ly/csc-first-year-academics
---
Now that midterms have been coming and going, some of you are probably feeling stressed and overwhelmed. Well that’s a normal feeling, and you’re not alone! Come join a panelist of upper years who understand your suffering, and can give insight on how to better succeed in first year academics.

Registration is optional; we’ll just be sending you a reminder on the day of, as well as a calendar invite. 

📅 Event Date: Oct.30th 2021, 2-3pm ET

💻 Zoom: 

Topic: CSC: Succeeding in First Year Academics Panel
Time: Oct 30, 2021 02:00 PM Eastern Time (US and Canada)

Join Zoom Meeting
https://us06web.zoom.us/j/89936812638?pwd=V1Q5Zk1idG1vc0JvMXpBMXFTVi85UT09

👉 Sign up using the register button. Alternatively, you can also email us at exec@csclub.uwaterloo.ca.

Hope to see you there! 🌸 
