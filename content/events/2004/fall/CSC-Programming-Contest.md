---
name: 'CSC Programming Contest'
short: 'CSC Programming Contest'
startDate: 'October 23 2004 23:00'
online: false
location: 'MC 2037'
---

The Computer Science Club will be hosting a programming competition.	You have the entire afternoon to design and implement an AI for a simple	game. The competition will run until 5pm.

