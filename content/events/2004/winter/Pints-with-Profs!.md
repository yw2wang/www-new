---
name: 'Pints with Profs!'
short: 'Get to know your profs and be the envy of your friends!'
startDate: 'March 30 2004 17:30'
online: false
location: 'The Grad House'
---

Come out and meet your professors!! This is a great opportunity to meet professors for Undergraduate Research jobs or to find out who you might have for future courses. One and all are welcome!

And best of all... free food!!!

