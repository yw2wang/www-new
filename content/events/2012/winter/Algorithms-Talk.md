---
name: 'Algorithms Talk'
short: '*by Victor Fan*. Join Victor Fan for his talk, intended for all second-year math students with a solid first-year background. Even if you are a first-year or a seasoned veteran, you will probably still take home something new, so please come out to enjoy the talk! Refreshments will be served.'
startDate: 'February 07 2012 18:00'
online: false
location: 'MC 4045'
---

Are you interested in algorithms? What is an algorithm anyway? We will discuss two or three neat problems with very elegant answers. Some of these answers are actually fast, and some will result in a proof that the problem is NP-complete. (What does that mean?) We will also discuss the motivating thoughts that led us to the solutions.

Join Victor Fan for his talk, intended for all second-year math students with a solid first-year background. Even if you are a first-year or a seasoned veteran, you will probably still take home something new, so please come out to enjoy the talk! Refreshments will be served.

