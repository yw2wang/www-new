---
name: 'OpenCL Introduction'
short: '*by Calum T. Dalek*. The University of Waterloo Computer Science Club and AMD are running an OpenCL programming competition. If you''re interested in writing massively parallel software on the OpenCL platform, come out and join us for our introductory code party!'
startDate: 'February 16 2012 19:00'
online: false
location: 'MC 3001'
---

The University of Waterloo Computer Science Club and AMD are running an [OpenCL programming competition.](<http://csclub.uwaterloo.ca/opencl>) If you're interested in writing massively parallel software on the OpenCL platform, come out and join us for our introductory code party!



