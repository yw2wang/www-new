---
name: 'CSC Code
Party 2'
short: 'We will be holding our second code party of the term. Watch for further details, as we plan on working with some robots and Scala, git, and Haskell.'
startDate: 'October 26 2012 19:00'
online: false
location: 'MC 3001'
---

We will be holding our second code party of the term. Watch for further details, as we plan on working with some robots and Scala, git, and Haskell.

