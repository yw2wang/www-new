---
name: 'UNIX 102: Tools in the UNIX Environment'
short: '*by Calum T. Dalek*. The next installment in the CS Club''s popular Unix tutorials UNIX 102 introduces powerful text editing tools for programming and document formatting.'
startDate: 'October 24 2011 16:30'
online: false
location: 'MC 3003'
---

Unix 102 is a follow up to Unix 101, requiring basic knowledge of the shell. If you missed Unix 101 but still know your way around you should be fine. Topics covered include: "real" editors, text processing, navigating a multiuser Unix environment, standard tools, and more. If you aren't interested or feel comfortable with these tasks, watch out for Unix 103 and 104 to get more depth in power programming tools on Unix.

