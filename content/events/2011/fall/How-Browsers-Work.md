---
name: 'How Browsers Work'
short: '*by Ehsan Akhgari*. Veteran Mozilla engineer Ehsan Akhgari will present a talk on the internals of web browsers. The material will range from the fundamentals of content rendering to the latest innovations in browser design. Click on the talk title for a full abstract.'
startDate: 'October 13 2011 18:30'
online: false
location: 'MC 4020'
---

Web browsers have evolved. From their humble beginnings as simple HTML rendering engines they have grown and evolved into rich application platforms. This talk will start with the fundamentals: how a browser creates an on-screen representation of the resources downloaded from the network. (Boring, right? But we have to start somewhere.) From there we'll get into the really exciting stuff: the latest innovations in Web browsers and how those innovations enable — even encourage — developers to build more complex applications than ever before. You'll see real-world examples of people building technologies on top of these "simple rendering engines" that seemed impossible a short time ago. Bio of the speaker: Ehsan Akhgari has contributed to the Mozilla project for more than 5 years. He has worked on various parts of Firefox, including the user interface and the rendering engine. He originally implemented Private Browsing in Firefox. Right now he's focusing on the editor component in the Firefox engine.

There will be 4 more code parties this term.

