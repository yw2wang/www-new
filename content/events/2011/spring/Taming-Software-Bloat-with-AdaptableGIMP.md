---
name: 'Taming Software Bloat with AdaptableGIMP'
short: '	Ever use software with 100s or 1000s of commands? Ever have a hard time	finding the right commands to perform your task? In this talk, we''ll	present AdaptableGIMP, a new version of GIMP developed at Waterloo to	help simplify complex user interfaces.	'
startDate: 'June 14 2011 16:30'
online: false
location: 'MC 2054'
---

Ever use software with 100s or 1000s of commands? Ever have a hard time	finding the right commands to perform your task? We have. And we have	some new ideas on how to deal with software bloat.

In this talk, we'll present AdaptableGIMP, a new version of GIMP	developed by the HCI Lab here at the University of Watreloo.	AdaptableGIMP introduces the notion of crowdsourced interface	customizations: Any user of the application can customize the interface	for performing a particular task, with that customization instantly	shared with all other users through a wiki at adaptablegimp.org. In the	talk, we'll demo this new version of GIMP and show how it can help	people work faster by simplifying feature-rich, complex user	interfaces.

