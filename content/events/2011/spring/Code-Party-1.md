---
name: 'Code Party 1'
short: '	The Computer Science Club is having our first code party of the term.	The theme for this week''s code party is personal projects. Come show us	what you''ve been working on! Of course, everybody is welcome, even if you	don''t have a project.	'
startDate: 'June 03 2011 19:00'
online: false
location: 'Comfy Lounge'
---

	The Computer Science Club is having our first code party of the term.	The theme for this week's code party is personal projects. Come show us	what you've been working on! Of course, everybody is welcome, even if you	don't have a project.	Personal projects are a great way to flex your CS muscles, and learn interesting	and new things. Come out and have some fun!	Two more are scheduled for later in the term.	