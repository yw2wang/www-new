---
name: 'General Purpose Computing on Graphics Cards'
short: '	In the first of our member talks for the term, Katie Hyatt will give a	short introduction to General Purpose Graphics Processing Unit	computing. This expanding field has many applications. The primary	focus of this talk will be nVidia''s CUDA architecture.	'
startDate: 'June 09 2011 16:30'
online: false
location: 'MC 2054'
---

This is the first of our member talks for the term, presented by	CSC member and Waterloo undergraduate student Katie Hyatt

GPGPU (general purpose graphics processing unit) computing is an	expanding area of interest, with applications in physics, chemistry,	applied math, finance, and other fields. nVidia has created an	architecture named CUDA to allow programmers to use graphics cards	without having to write PTX assembly or understand OpenGL. CUDA is	designed to allow for high-performance parallel computation controlled	from the CPU while granting the user fine control over the behaviour	and performance of the device.

In this talk, I'll discuss the basics of nVidia's CUDA architecture	(with most emphasis on the CUDA C extensions), the GPGPU programming	environment, optimizing code written for the graphics card, algorithms	with noteworthy performance on GPU, libraries and tools available to	the GPGPU programmer, and some applications to condensed matter	physics. No physics background required!

