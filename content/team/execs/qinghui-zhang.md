---
name: Sean Zhang
role: Assistant Vice-President
---

- Finished 4A CS, currently on co-op
- S24 AVP, S23 OM
- Find me in the CSC Discord server
- Some hobbies: piano, board games, hiking, reading
- Ask me about: my ortho split ergo mech keyboard, trains
