---
name: Gordon Lin
role: President
---

Heyo! I'm Gordon (he/him/his) and I'm the S24 CSC President! That means I oversee all the administrative tasks and make sure that things are great for both the CSC team and community!

A bit about me:

- Studying Computer Science (Bioinformatics Specialization) & Combinatorics and Optimization
- Just finished my 2B term, currently on co-op
- Fascinated about the world, love learning new things especially in computer science, math, and biology
- Enjoys playing strategy video/board games (any HOI4 players?), photography (main Sony ZV-E10), crochet (just made a goose plush!), music (clarinet player), and socializing

Feel free to say hi! I would love to chat at the next CSC event, in the office, or on the CSC Discord!
