---
author: 'hkarau'
date: 'June 04 2006 01:00'
---

The BeBox lives again. BeOS is a unique operating system from the same era as Windows 95. If you'd like to see it run, port something to it (we have the developement tools!), or just watch the funny lights on the front of it, stop by the office and take a look