---
author: 'ebering'
date: 'May 10 2010 01:00'
---

Nominations for the spring term executive are closed. Nominees are: - President: Jeff Cameron (j3camero), Kevin Farnworth (kfarnwor)
- Vice President: Brennan Taylor (b4taylor)
- Treasurer: Vardhan Mudunuru (vmudunur)
- Secretary: Matt Lawrence (m3lawren), Qifan Xi (qxi)

<!-- -->

 Elections will occur as listed in the events section for the two contested positions. If you would like to vote absentee, please send your votes (one vote per position) to [the Chief Returning Officer](<mailto:cro@csclub.uwaterloo.ca>) before 5:30 PM EDT on May 11, 2010.