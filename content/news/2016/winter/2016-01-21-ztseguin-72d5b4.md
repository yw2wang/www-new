---
author: 'ztseguin'
date: 'January 21 2016 00:00'
---

The [Computer Science Club Mirror](<http://mirror.csclub.uwaterloo.ca>) is now running on potassium-benzoate, our new server which was purchased with funding from the [Mathematics Endowment Fund (MEF)](<https://www.student.math.uwaterloo.ca/~mefcom/>). We have also enhanced our mirror's network connectivity, adding IPv6 and 10Gbps.

Running our mirror is made possible with assistance from:

- The [Mathematics Endowment Fund (MEF)](<https://www.student.math.uwaterloo.ca/~mefcom/>)
- The [Mathematics Society (MathSoc)](<http://mathsoc.uwaterloo.ca>)
- Dave Gawley and the [Computer Science Computing Facility (CSCF)](<https://cs.uwaterloo.ca/resources-services/cscf>)
- [Information Systems Technology (IST)](<https://uwaterloo.ca/information-systems-technology/>)

<!-- -->