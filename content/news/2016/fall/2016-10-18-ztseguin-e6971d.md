---
author: 'ztseguin'
date: 'October 18 2016 01:00'
---

Due to a planned power outage in the Mathematics & Computer Building on Sat. Oct 22 from 7am to 11pm, Computer Science Club systems and services will be unavailable.

It is expected that our Open Source Software mirror ([mirror.csclub.uwaterloo.ca](<http://mirror.csclub.uwaterloo.ca>)) remain available during the outage. This is made possible with assistance from the Computer Science Computing Facility (CSCF).