---
author: 'abandali'
date: 'July 27 2020 01:00'
---

The CSC Systems Committee announces the availability of a new web IRC client for CSC members at [chat.csclub.uwaterloo.ca](<https://chat.csclub.uwaterloo.ca>), as the first of several steps it is taking to bring modern user freedom- and privacy-respecting communication tools to CSC members. Please see the [notice](<https://mailman.csclub.uwaterloo.ca/pipermail/csc-general/2020-July/000837.html>) sent to csc-general for the complete announcement.