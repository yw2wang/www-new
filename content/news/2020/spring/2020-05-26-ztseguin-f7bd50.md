---
author: 'ztseguin'
date: 'May 26 2020 01:00'
---

The Systems Committee has been informed of a planned power outage in the Math & Computer Building (MC) on Thursday, May 28 from 10am to 12pm (noon) EDT. Club systems and services will be unavialble starting at 8am EDT. See the [announcement](<https://mailman.csclub.uwaterloo.ca/pipermail/csc-general/2020-May/000835.html>) sent to csc-general for more information.