---
author: 'pj2melan'
date: 'May 14 2018 01:00'
---

Here are the nominations for the Spring 2018 elections:

- President: Melissa (matedesc), Haoran (h594wang)
- VP: Dhruv (djauhar), Haoran (h594wang)
- Secretary: Dhruv (djauhar), Marc (mnmailho)
- Treasurer: Tristan (tghume)

<!-- -->

Additionally, the following people have expressed interest in appointed positions:

- Office Manager: Aditya (a3thakra), Archer (z577zhan), Haoran (h594wang), Yash (y2mathur)
- Librarian: Aditya (a3thakra), Archer (z577zhan), Anamaya (agarodia)
- Pop/Fridge Regent: Marc (mnmailho)
- Sysadmin: Jennifer (c7zou)

<!-- -->