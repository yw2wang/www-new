---
author: 'ztseguin'
date: 'July 18 2018 01:00'
---

The Systems Committee has been informed of a planned power outage in the Math & Computer Building (MC) Tuesday, August 21 to Thursday, August 30. Systems will be unavailable starting Sunday, August 19 at 12pm EDT and are expected to be restored on Thursday, August 30.

We expect that our [ Open Source Software mirror ](<http://mirror.csclub.uwaterloo.ca/>)be available during the outage.

See the [ notice ](<https://mailman.csclub.uwaterloo.ca/pipermail/csc-general/2018-August/000769.html>) sent to csc-general for more information.

Please contact the [ Systems Committee ](<mailto:systems-committee@csclub.uwaterloo.ca>) if you have any questions.

*Updated on 2018-07-19 as the outage time was extended.*

*Updated on 2018-08-02 with additional information and extended outage time.*