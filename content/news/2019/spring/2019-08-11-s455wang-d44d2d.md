---
author: 's455wang'
date: 'August 11 2019 01:00'
---

There will be a power outage on campus from 7:00 am until 9:00 pm on Sunday, August 18th (EDT). To prepare for this outage, all Computer Science Club servers will be down starting at 11:00 PM on Saturday, August 17th. This includes web hosting, CSC Cloud, email, and mirror. Please make sure you save all of your work before then, and we also recommend backing up your files just in case.

See also [the notice from Plant Ops](<https://uwaterloo.ca/plant-operations/news/service-interruption-electricity-multiple-buildings-august>).