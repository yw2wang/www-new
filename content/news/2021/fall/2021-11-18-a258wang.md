---
author: 'a258wang'
date: 'November 18 2021 00:00'
---
📣 CS Club’s Internal Committee is hiring for Winter 2022! 📣 

Are you interested in organizing program-wide events, reaching out to industry professionals, or being a member of an impactful community? Are you passionate about making a difference in the UW CS and surrounding communities?

Apply for a role on CS Club's Internal Committee for Winter 2021! We are looking for people like you to join our Programme Committee and/or Systems Committee! 🙌 

👀  Role descriptions can be found at https://bit.ly/uwcsclub-w22-roles.

⏲️ We'll be sending out interviews on a ** ROLLING BASIS **, so keep your eyes peeled and apply ASAP! We'll reach out through email for additional interviews/questions.

👉  Apply at https://bit.ly/uwcsclub-w22-apply! Alternatively, you can email us at exec@csclub.uwaterloo.ca from your UW email with an introduction of yourself, which positions you're interested in and any questions you might have.
