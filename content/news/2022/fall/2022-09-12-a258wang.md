---
author: 'a258wang'
date: 'September 12 2022 00:00'
---

Fall 2022 elections have concluded. Here are your executives for the term:

- President: Amy Wang (a258wang)
- Vice President: Anna Wang (aj2wang)
- Assistant Vice President: Mabel Kwok (m23kwok)
- Treasurer: Simon Zeng (s33zeng)
- Sysadmin: Raymond Li (r389li)
