---
author: 'j285he'
date: 'September 7 2022 00:00'
---
The Computer Science Club will be holding elections for Fall 2022 in
person, on Monday September 12 at 7PM in MC 2017. The
president, vice-president, treasurer, and assistant vice-president
(formerly secretary) will be elected, and the sysadmin will be appointed.

If you'd like to run for any of these positions or nominate someone, you
can send an email to cro@csclub.uwaterloo.ca, or present them in-person
to the CRO, Juthika Hoque, or write your name on the whiteboard in the
CSC office (MC 3036/3037). Nominations will close on Monday September 12
at 5PM.

Another email will be sent out closer to the election date for the
specific location. If you have any further questions about elections or
nominations, please email cro@csclub.uwaterloo.ca.

---

syscom is pleased to announce the release of two new services: CSC Files
and CSC Links.

CSC Files is our new Nextcloud instance accessible at
https://files.csclub.uwaterloo.ca through CSC's Keycloak SSO. It's
basically self-hosted Google Drive, and every member has 50GB of storage
available to them. The following notable non-default Nextcloud apps are
currently installed (subject to change):
- Audio/Video Players, PDF viewers, etc.
- Collabora office (with CODE server, self-hosted GSuite/MS Office in
the browser)
- Cospend (self-hosted Splitwise, a cost-sharing app for friend groups)
- Extract (extract zip files)
- Forms (self-hosted Google/Microsoft Forms)
- Full text search (for files)
- Google integration (for migration from Google Drive)
- ShareRenamer (custom share URLs)
- Transfer/NCDownloader (allows downloading videos, torrents and other
files on the internet directly to your CSC Files account without having
to download to a local computer and then re-upload it)
NOTE: usage of this app (and all of CSC Files) is still covered by the
Machine Usage Agreement, you may not do anything illegal with it.

If you need more storage or would like another app installed, please
email a proposal including a legitimate reason to
syscom@csclub.uwaterloo.ca and we will consider your request.

CSC Links is our new Shlink instance, a self-hosted URL shortener.
Creating/managing shortlinks is currently limited to execs, but may
change in the future. Official CSC shortlinks will now be exclusively
served from the domain csclub.ca (i.e. https://csclub.ca/discord)

---

syscom is switching over the default PHP version on caffeine from 7.4
(going out of support in November) to 8.2 around midnight between
Saturday August 27th and Sunday August 28th. Please update any PHP
websites accordingly.
