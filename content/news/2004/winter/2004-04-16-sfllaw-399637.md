---
author: 'sfllaw'
date: 'April 16 2004 01:00'
---

`carbonated-water` is back up. It now runs as the backup mail exchange and also serves a a slave LDAP server. What this means is that if `perpugilliam` goes down, your mail will not be dropped and you'll still be able to login to other machines.