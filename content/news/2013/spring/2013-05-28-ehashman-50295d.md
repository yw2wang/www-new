---
author: 'ehashman'
date: 'May 28 2013 01:00'
---

Consider attending the Canadian Undergraduate Mathematics Conference in Montreal, Quebec this semester, July 10—14. Funding for transportation and accommodations has been generously provided by MEF, MathSoc, and the Dean's Office.

To apply for funding, please fill out [this form](<https://docs.google.com/forms/d/1rKAdBmGWRj0VzlxVA1s5pLmeMYQve2EE4F76hhTDwBc/viewform>).

If you have any questions, please contact me directly.