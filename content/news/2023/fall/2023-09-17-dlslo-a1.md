---
author: 'dlslo'
date: 'September 17 2023 00:00'
---

The Fall 2023 Elections were held Tuesday, September 12th at 6PM EDT. Here are the elected executives for the term!

- President: Laura Nguyen (l69nguye)
- Vice-President: Amol Venkataraman (avenkata)
- Assistant Vice-President: Amy Wang (a258wang)
- Treasurer: Bryan Chen (b28chen)
- Sysadmin: Nathan Chung (n4chung)
