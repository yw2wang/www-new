---
author: 'shahanneda'
date: 'Feb 14 2023 00:00'
---

🎊 After a much anticipated wait, the 2022 CS Class Profile is finally here! 🎊

🧠 Have you ever wondered what graduating CS, CS/BBA, and CFM students’ favorite company to co-op at is? Or just how many hours of sleep they really get? Well, wonder no more because all those questions and more are answered in the Class Profile!

⭐ Thank you to everyone who filled out the survey and represented our Class of 2022, we couldn’t have done it without you!

👉 Check out the Class Profile here: https://csclub.uwaterloo.ca/classprofile/2022
