---
author: 'm4burns'
date: 'June 06 2012 01:00'
---

To all of our members who are interested in attending CUMC at UBCO this summer, please remember to [apply for funding!](<https://docs.google.com/spreadsheet/viewform?formkey=dG56d3NRNUJabzQyMmpNTDlqWkpiU3c6MQ>)