---
author: 'ztseguin'
date: 'May 17 2017 01:00'
---

Here are the results from this term's elections:

- President: fbauckho (Felix Bauckholt)
- Vice-President: z34wei (Zichuan Wei)
- Treasurer: lhsong (Laura Song)
- Secretary: bzmo (Bo Mo)

<!-- -->

Additionally, the following positions were appointed:

- Systems Administrator: ztseguin (Zachary Seguin)
- Office Manager: ubarar (Uday Barar)
- Librarian: pj2melan (Patrick Melanson)
- Imapd: ubarar (Uday Barar)

<!-- -->