---
author: 'b2coutts'
date: 'May 15 2014 01:00'
---

Elections for Spring 2014 have concluded. The following people were elected:

- President: Jinny Kim (`yj7kim`)
- Vice-president: Luke Franceschini (`l3france`)
- Treasurer: Joseph Chouinard (`jchouina`)
- Secretary: Ifaz Kabir (`ikabir`)

<!-- -->

The Sysadmin and Office Manager were not appointed at the election.