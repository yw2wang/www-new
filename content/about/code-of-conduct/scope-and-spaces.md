---
title: Scope and Spaces
---

In cases where the Code of Conduct contradicts University policies, or applicable laws and bylaws, the Code of Conduct does not apply to the extent to which it conflicts.

We expect all Club participants (participants, organizers, sponsors, and other guests) to abide by this Code of Conduct in all community venues (online and in-person) as well as in all one-on-one communications pertaining to Club business.

- The Code of Conduct applies in the office, where office staff are responsible for enforcing it.
- The Code of Conduct applies in the IRC channel, where channel operators are responsible for enforcing it.
- The Code of Conduct applies in the Discord channel, where moderators are responsible for enforcing it.
- The Code of Conduct applies at events the CSC organizes or co-organizes, where a designated organizer is responsible for enforcing it.
