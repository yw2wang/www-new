---
title: Experiencing Unacceptable Behaviour
---

_The Executive Council and Faculty Advisor are herein referred to as the Officers, or singularly as Officer._

If you notice a dangerous situation, someone in distress, or violations of this Code of Conduct, [contact an Officer](mailto:coc@csclub.uwaterloo.ca). No situation is considered inconsequential. If you do not feel comfortable contacting an Executive Council member due to the nature of the incident, you may contact the faculty advisor, [Dr. Prabhakar Ragde](https://uwaterloo.ca/computer-science/about/people/plragde).

Upon receiving a complaint the Officer will inform the first of the following people who is not personally involved in the situation, or in a close relationship with the someone involved in the situation and is available, and this person shall handle the complaint and shall here after be referred to as the Handling Officer.

1. The President
2. The Vice President
3. Any other Executive Council Member
4. The Faculty Advisor

The Handling Officer will interview the subject of the complaint and any witnesses and consult with other relevant Officers. The Handling Officer shall chair a handling committee of the Faculty Advisor and one other officer chosen in the same way. This committee shall be familiar with University Policies 33, 34, and 42.

The Faculty Advisor will make sure that all applicable University policies, laws, and bylaws are followed. The Faculty Advisor must always be notified of all complaints and decisions.
