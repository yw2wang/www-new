---
title: Expected Behaviour
---

- Participate in an authentic and active way. In doing so, you contribute to the health and longevity of this Club.
- Exercise consideration and respect in your speech and actions.
- Attempt collaboration before conflict.
- Refrain from demeaning, discriminatory, or harassing behaviour and speech.
- Be mindful of your surroundings and of your fellow participants.
