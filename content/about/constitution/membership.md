---
title: Membership
---

1. In compliance with MathSoc regulations and in recognition of the club being primarily targeted at undergraduate students, full membership is open to all Social Members of the Mathematics Society and restricted to the same.
2. Affiliate membership in this Club shall be open to all members of the University community, including alumni. Affiliate members shall have all the rights of full members except for the rights of voting and holding executive office.
3. Membership shall be accounted for on a termly basis, where a term begins at the start of lectures in Winter or Spring, and at the start of Orientation Week in Fall.
4. A person is not a member until he or she has paid the current membership fee and has been enrolled in the member database. A person must only be enrolled after paying the current membership fee, unless they are not a current undergraduate student at the University and the Executive grants them honorary membership free of charge. The termly membership fee is set from time to time by the Executive. Under conditions approved by the Executive, a member who purchases a membership at the end of the current term may be given membership for both the current term and the next term. If the membership fee changes, then this does not affect the validity of any membership terms already paid for.
5. The Club may grant access to its systems, either free of charge or for a fee, to members of the University community in order to offer them services. This does not constitute membership.
