---
title: Officers
---

1. The officers of the Club shall be:
    1. President
    1. Vice-President
    1. Assistant Vice-President
    1. Treasurer
    1. Systems Administrator
1. There shall additionally be a Faculty Advisor, selected by the Executive from time to time from among the faculty of the School of Computer Science. The Faculty Advisor shall be an ex-officio affiliate member of the Club.
1. The choice of officers shall be limited to full members of the Club.
1. All officers, other than the Systems Administrator, shall be elected at a meeting to be held no later than two weeks after the start of lectures in each term.
1. The election of officers shall be accomplished by the following procedure:
    1. Before the end of the prior term, the then-Executive shall choose a willing Chief Returning Officer, who is responsible for carrying out elections according to this procedure.
    1. The CRO shall set the date and time of the election meeting, and set the nomination period. The nomination shall be at least one week long and shall end at least 24 hours before the start of the election meeting.
    1. Announcements of the election and the nomination procedure must be distributed to all members by the members' mailing list.
    1. During the nomination period, the Chief Returning Officer (CRO) shall be available to receive nominations for the posts of officers of the club, either in person, by email, or by writing the nomination in a place in the CSC office to be specified by the CRO.
    1. A nomination shall consist of the nominee's userid, and post(s) nominated for. Nominees must be full members of the Computer Science Club. A member may decline a nomination at any point prior to the taking of the vote.
    1. Within 24 hours of the CRO receiving a nomination, the CRO must publicize the nomination, such as by writing it on the CSC office whiteboard.
    1. Each nominee shall make a platform and submit it to the CRO. Within 24 hours of the CRO receiving a platform from a nominee, the CRO must publicize the platform. Each nominee is also encouraged to publicize their platform on their own.
    1. The election shall commence with the offering of memberships for sale. After a reasonable time, control of the meeting is given to the CRO who will preside over the election of the President, Vice-President, Assistant Vice-President, and Treasurer, in that order.
    1. All nominees shall present their platforms. If a position has no nominees, then the CRO shall take nominations from the floor. Any present, eligible member can be nominated.
    1. Voting shall be by secret ballot, in a manner that is to be decided on by the CRO and agreed upon by the members at the meeting. A simple heads-down-hands-up method is considered acceptable.
    1. The CRO shall not vote except to break a tie.
    1. The CRO may, if feasible, accept absentee ballots from full members. No absentee vote from a member shall be counted if the member is present at the time the vote is taken. The CRO shall make a best effort to ensure that absentee ballots are compatible with the method of voting chosen; if this is not possible (for instance, if the CRO is overruled by the membership), then the absentee votes shall not be counted.
    1. Immediately after the vote is taken, the CRO will announce the results of the election and the winner will be removed from subsequent contests. If, due to a lack of candidates (because there were no nominations, or candidates withdrew or were eliminated), there is no one elected to an office, then the members at the meeting will decide whether or not to hold extra elections in accordance with the procedure for vacancies. If they choose not to, this does not prevent the Executive or a group of members from calling extra elections later in the term in accordance with the usual vacancy provisions.
1. Following the elections, it is the responsibility of the new Executive to select a Systems Administrator. The selection of Systems Administrator must then be ratified by the members at the meeting. If a suitable Systems Administrator is not available, the duties of the Systems Administrator shall be carried out by the Systems Committee, with decisions made by consensus achieved on their mailing list.
1. Any two offices may be held by a single person with the approval of the President (if any), and the explicit approval of the members.
1. In the case of a resignation of an officer or officers, including the President, or if a vacancy occurs for any other reason, the Executive, members at a meeting, or any ten (10) members may call extra elections to replace such officer(s). If extra elections are held, they are held for all vacant offices.
1. Whenever extra elections are held, they shall follow the usual election procedure. If they are held after elections failed to elect an officer, then the nomination period may be shortened to less than a week in order to allow the extra elections to take place at the same date and time in the following week. The Executive (or the ten (10) members who called the election) may appoint a replacement CRO if the previous CRO is unwilling or unable to fulfill their duties.
