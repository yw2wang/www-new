---
title: Club Accounts
---

The club accounts policy is divided into the following 2 sections:

1. Access Control
1. Responsibility and Accountability

## Access Control

_Note: For the given section, any mention of the club, except in direct reference to the Computer Science Club, will refer to a club other than the CSC, which has, or requests, an account on a Computer Science Club machine._

Clubs are given accounts and provided with an e-mail and WWW pages, but are subject to the following to certain rules. They are as follows:

1. The club account is subject to all restrictions of a user account, except that it is a shareable account.
1. The club members must have regular user accounts on the CSC machine that the club account will be on. If the club member does not already have such an account, one will be created to allow the member to manage the club account.
1. The members of the club with access to the club account shall be known to the CSC Systems Administrator to ensure that these people are aware of this section of the user agreement.
1. The club members with access to the club account shall not grant access to any other members by any means that are available to them, other than approaching the CSC System Administrator and requesting the change of access.

## Responsibility and Accountability

The account is the responsibility of the members who have access. If the resources owned by the club account are found to be in violation of any policy/rule/law of any of, but not limited to, the Computer Science Club, CSCF, the University of Waterloo, or any relevant law enforcement agency, then the members with access will be held **equally** responsible for that action.

Club reps are required to read the [Club Hosting](https://wiki.csclub.uwaterloo.ca/Club_Hosting) page on the CSC Wiki when they first become a club rep, and thereafter at least once every term, and abide by any notices, warnings, recommendations and best practices provided on it.
