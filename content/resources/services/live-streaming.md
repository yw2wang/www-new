---
title: Live Streaming
---

We host an instance of [Icecast](https://icy.csclub.uwaterloo.ca/), which can stream live audio and video. We have successfully streamed live events to Icecast using OBS Studio. Latency usually ranges between 5-10 sec.

If you are interested in live streaming via Icecast, please contact the [Systems Committee](mailto:syscom@csclub.uwaterloo.ca) (syscom at csclub dot uwaterloo dot ca).
