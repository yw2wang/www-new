---
title: Cloud Accounts
---

With the CSC cloud, you can create your own virtual machines, host your Docker container images, and deploy your apps on Kubernetes.

See [https://docs.cloud.csclub.uwaterloo.ca/](https://docs.cloud.csclub.uwaterloo.ca/) for more details.
