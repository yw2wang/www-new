---
title: Git Hosting
---

We host an instance of [Gitea](https://git.csclub.uwaterloo.ca/) for all of our members. The UI and workflow of Gitea are very similar to GitHub. Gitea is currently home to most CSC software projects, including the [code for this website](https://git.csclub.uwaterloo.ca/www/www-new).

For more information, see [this page](https://wiki.csclub.uwaterloo.ca/Git_Hosting) on our wiki.

We also host an instance of [drone.io](https://ci.csclub.uwaterloo.ca/) for CI/CD. To use Drone, you need to login to Gitea first. You can then selectively enable CI on your repos from the Drone UI. See the [Drone documentation](https://docs.drone.io) for more information.
