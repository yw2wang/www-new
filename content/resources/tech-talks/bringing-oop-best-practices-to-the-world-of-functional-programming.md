---
index: 4
title: 'Bringing OOP Best Practices to the World of Functional Programming'
presentors:
  - Elana Hashman
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/ehashman-oop-best-practices-thumb-small.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/ehashman-oop-best-practices.mp4'
    type: 'OOP Best Practices (mp4)'
---

I transitioned from writing software in imperative, object-oriented (OO) programming languages to doing functional programming (FP) full-time, and you can do it, too! In this talk, I'll make a case for using FP for real-world development, cover some cases where common FP language features substitute for design patterns and OOP structure, and provide some examples of translating traditional OO design patterns into functional code.

Due to battery shenanigans, not the entire talk was recorded. Instead, you can get the slides for this talk at [the talks section of her site](<https://hashman.ca/osb-2016/>).
