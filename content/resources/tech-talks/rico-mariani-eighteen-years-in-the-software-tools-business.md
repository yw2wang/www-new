---
index: 62
title: 'Rico Mariani: Eighteen Years in the Software Tools Business'
presentors:
  - Rico Mariani
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/rico-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/rico-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/rico.avi'
    type: 'XviD'
    size: '534M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/rico.ogg'
    type: 'Ogg/Theora'
    size: '528M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/rico.mp4'
    type: 'MP4'
    size: '507M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/rico.mpg'
    type: 'MPG'
    size: '532M'
---

Rico Mariani, (BMath CS/EEE 1988) now an (almost) 18 year Microsoft veteran but then a CSC president comes to talk to us about the evolution of software tools for microcomputers. This talk promises to be a little bit about history and perspective (at least from the Microsoft side of things) as well as the evolution of software engineers, different types of programmers and their needs, and what it's like to try to make the software industry more effective at what it does, and sometimes succeed! Particularly illuminating are his responses to advocates of free/open-source software.
