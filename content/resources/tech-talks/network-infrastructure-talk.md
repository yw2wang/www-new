---
index: 6
title: 'Network Infrastructure talk'
presentors:
  - Steve Bourque and Mike Patterson
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/uw-infrastructure-sbourque-half-thumb-small.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/uw-infrastructure-sbourque-half.mp4'
    type: 'Steven Bourque Talk (x264)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/uw-infrastructure-mpatters-half.mp4'
    type: 'Mike Patterson Talk (x264)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/uw-infrastructure-mpatters-slides.pdf'
    type: 'Mike Patterson Slides (pdf)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/uw-infrastructure-sbourque-slides.pdf'
    type: 'Steven Bourque Slides (pdf)'
---

Steve Bourque and Mike Patterson of IST will give a brief overview of campus network connectivity and interconnectivity. Steve will describe the general connections, and Mike will talk about specific security measures in place. We'll have refreshments!
