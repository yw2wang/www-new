---
index: 5
title: 'Open Source Computer Sound Measurement'
presentors:
  - Richard Mann
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/rmann-oss-sound-measurement-thumb-small.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/rmann-oss-sound-measurement.mp4'
    type: 'OSS Sound Measurement (mp4)'
---

An ideal computer audio system should faithfully reproduce signals of all frequencies in the audible range (20 to 20,000 cycles per second). Real systems, particularly mobile devices and laptops, may still produce acceptable quality, but often have a limited response, particularly at the low (bass) frequencies. Sound/acousic energy refers to time varying pressure waves in air. When recording sound, the acoustic signal will be picked up by microphone, which converts it to electrical signals (voltages). The signal is then digitized (analog to digital conversion) and stored as a stream of numbers in a data file. On playback the digital signal is converted to an electrical signal (digital to analog conversion) and finally returned as an acoustic signal by a speaker and/or headphones. In this talk I will present open source software (Octave/Linux) to measure the end-to-end frequency response of an audio system using the Discrete Fourier Transform. I will demonstrate the software using a standard USB audio interface and a consumer grade omnidirectional microphone. This is joint work with John Vanderkooy, Distinguished Professor Emeritus, Department of Physics and Astronomy.
