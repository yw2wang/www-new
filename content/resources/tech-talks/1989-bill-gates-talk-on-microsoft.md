---
index: 55
title: '1989 Bill Gates Talk on Microsoft'
presentors:
  - Bill Gates
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/audio-file.png'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/audio-file.png'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/bill-gates-1989.mp3'
    type: 'mp3'
    size: '85M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/bill-gates-1989.flac'
    type: 'flac'
    size: '540M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/bill-gates-1989.ogg'
    type: 'ogg'
    size: '56M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/bill-gates-1989.wav'
    type: 'wav'
    size: '945M'
---

Bill Gates discusses the software and computer industry, and how Microsoft has contributed. Gates also discusses his views on the future of the computing industry. The talk was recorded in 1989 but was only recently digitized.

Topics include:

- The start and history of the microcomputer industry
- Microsoft BASIC and the Altair 880 computer
- The transition from 8-bit to 16-bit computers
- Microsoft's history with IBM
- 640k memory barrier and 16-bit architectures
- 32-bit 386 and 486 architectures
- RISC and multi-processor machines
- EGA graphics and WYSIWYG editors
- Decreasing cost of memory, harddisks and hardware in general
- The importance and future of the mouse
- Object-oriented programming
- MS-DOS and OS/2
- Multi-threaded and multi-application systems
- Synchronization in multi-threaded applications
- Diskette-based software
- UNIX standardization and POSIX
- History of the Macintosh and Microsoft' involvement
- Involvement of Xerox in graphical user interfaces
- Apple vs. Microsoft lawsuit regarding user interfaces
- OS/2 future as a replacement for MS-DOS
- Microsoft Office on Macintosh
- Thin/dumb clients
- Compact discs
- Multimedia applications
- Gates' current role at Microsoft

<!-- -->

The following picture was taken after the talk (click for higher-res).

[![null](<http://mirror.csclub.uwaterloo.ca/csclub/bill-gates-1989.jpg>)](<http://mirror.csclub.uwaterloo.ca/csclub/bill-gates-1989-big.jpg>)
