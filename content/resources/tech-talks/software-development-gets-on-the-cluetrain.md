---
index: 57
title: 'Software development gets on the Cluetrain'
presentors:
  - Simon Law
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/simon-talk-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/simon-talk-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/simon-talk-xvid.avi'
    type: 'XviD'
    size: '178M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/simon-talk.avi'
    type: 'DivX'
    size: '178M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/simon-talk.mpg'
    type: 'MPG'
    size: '177M'
---

Simon Law leads the Quality teams for Ubuntu, a free-software operating system built on Debian GNU/Linux. As such, he leads one of the largest community-based testing efforts for a software product. This does get a bit busy sometimes.

In this talk, we'll be exploring how the Internet is changing how software is developed. Concepts like open source and technologies like message forums are blurring the lines between producer and consumer. And this melting pot of people is causing people to take note, and changing the way they sling code.

The Computer Science Club would like to thank the CS-Commons Committee for co-sponsoring this talk.
