---
index: 36
title: 'An Introduction to Vector Graphics Libraries with Cairo'
presentors:
  - Nathaniel Sherry
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/nsasherr-cairo-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/nsasherr-cairo-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/nsasherr-cairo.avi'
    type: 'Talk (XviD)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/nsasherr-cairo.ogg'
    type: 'Talk (Ogg/Theora)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/nsasherr-cairo.mp4'
    type: 'Talk (MP4)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/nsasherr-cairo.mpg'
    type: 'Talk (MPG)'
---

Cairo is an open source, cross platform, vector graphics library with the ability to output to many kinds of surfaces, including PDF, SVG and PNG surfaces, as well as X-Window, Win32 and Quartz 2D backends.

Unlike the raster graphics used with programmes and libraries such as The Gimp and ImageMagick, vector graphics are not defined by grids of pixels, but rather by a collection of drawing operations. These operations detail how to draw lines, fill shapes, and even set text to create the desired image. This has the advantages of being infinitely scalable, smaller in file size, and simpler to express within a computer programme.

This talk will be an introduction to the concepts and metaphors used by vector graphics libraries in general and Cairo in particular.
