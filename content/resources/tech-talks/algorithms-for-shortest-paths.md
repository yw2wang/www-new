---
index: 11
title: 'Algorithms for Shortest Paths'
presentors:
  - Anna Lubiw
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/alubiw-shortest-paths-thumb-small.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/alubiw-shortest-paths.mp4'
    type: 'Talk (x264)'
---

Finding shortest paths is a problem that comes up in many applications: Google maps, network routing, motion planning, connectivity in social networks, and etc. The domain may be a graph, either explicitly or implicitly represented, or a geometric space.

Professor Lubiw will survey the field, from Dijkstra's foundational algorithm to current results and open problems. There will be lots of pictures and lots of ideas.
