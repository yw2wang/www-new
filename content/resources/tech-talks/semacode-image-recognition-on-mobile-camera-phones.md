---
index: 58
title: 'Semacode - Image recognition on mobile camera phones'
presentors:
  - Simon Woodside
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/semacode-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/semacode-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/semacode.avi'
    type: 'DivX'
    size: '180M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/semacode-xvid.avi'
    type: 'XviD'
    size: '180M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/semacode.mpg'
    type: 'Mpeg'
    size: '180M'
---

Could you write a good image recognizer for a 100 MHz mobile phone processor with 1 MB heap, 320x240 image, on a poorly-optimized Java stack? It needs to locate and read two-dimensional barcodes made up of square modules which might be no more than a few pixels in size. We had to do that in order to establish Semacode, a local start up company that makes a software barcode reader for cell phones. The applications vary from ubiquitous computing to advertising. Simon Woodside (founder) will discuss what it's like to start a business and how the imaging code works.
