---
index: 38
title: 'Programming Quantum Computers'
presentors:
  - Dr. Raymond Laflemme and Various
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/iqc1-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/iqc1-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/iqc1.avi'
    type: 'Talk (XviD)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/iqc1.ogg'
    type: 'Talk (Ogg/Theora)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/iqc1.mp4'
    type: 'Talk (MP4)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/iqc1.mpg'
    type: 'Talk (MPG)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/iqc2.avi'
    type: 'Quantum Key Distribution Lab (XviD)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/iqc2.ogg'
    type: 'Quantum Key Distribution Lab (Ogg/Theora)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/iqc2.mp4'
    type: 'Quantum Key Distribution Lab (MP4)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/iqc2.mpg'
    type: 'Quantum Key Distribution Lab (MPG)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/iqc3.avi'
    type: 'NMR Quantum Computer (XviD)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/iqc3.ogg'
    type: 'NMR Quantum Computer (Ogg/Theora)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/iqc3.mp4'
    type: 'NMR Quantum Computer (MP4)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/iqc3.mpg'
    type: 'NMR Quantum Computer (MPG)'
---

Raymond Laflamme is the director of the Institute for Quantum Computing at the University of Waterloo and holds the Canada Research Chair in Quantum Information. He will give a brief introduction to quantum computing and why it matters, followed by a talk on programming quantum computers. This is followed by tours of IQC Labs.
