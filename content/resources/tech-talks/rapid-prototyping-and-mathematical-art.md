---
index: 40
title: 'Rapid Prototyping and Mathematical Art'
presentors:
  - Dr. Craig Kaplan
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/kaplan-mathematical-art-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/kaplan-mathematical-art-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/kaplan-mathematical-art.avi'
    type: 'XviD'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/kaplan-mathematical-art.ogg'
    type: 'Ogg/Theora'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/kaplan-mathematical-art.mp4'
    type: 'MP4'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/kaplan-mathematical-art.mpg'
    type: 'MPG'
---

The combination of computer graphics, geometry, and rapid prototyping technology has created a wide range of exciting opportunities for using the computer as a medium for creative expression. In this talk, I will describe the most popular technologies for computer-aided manufacturing, discuss applications of these devices in art and design, and survey the work of contemporary artists working in the area (with a focus on mathematical art). The talk will be primarily non-technical, but I will mention some of the mathematical and computational techniques that come into play.

The slides for this talk can be found [here](<http://mirror.csclub.uwaterloo.ca/csclub/kaplan-mathematical-art-slides.pdf>) as a pdf.
