---
index: 28
title: 'The Art of the Propagator'
presentors:
  - Gerald Jay Sussman
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/sussman-propagator-thumb-small.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/sussman-propagator.mkv'
    type: 'Talk (MKV)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/sussman-propagator-slides.pdf'
    type: 'Slides (PDF)'
---

We develop a programming model built on the idea that the basic computational elements are autonomous machines interconnected by shared cells through which they communicate. Each machine continuously examines the cells it is interested in, and adds information to some based on deductions it can make from information from the others. This model makes it easy to smoothly combine expression-oriented and constraint-based programming; it also easily accommodates implicit incremental distributed search in ordinary programs. This work builds on the original research of Guy Lewis Steele Jr. and was developed more recently with the help of Chris Hanson.
