---
index: 47
title: 'Copyright vs Community in the Age of Computer Networks'
presentors:
  - Richard M. Stallman
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/rms-qa-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/rms-qa-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/rms-talk.ogg'
    type: 'Talk (Ogg/Theora)'
    size: '687M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/rms-qa.ogg'
    type: 'Q&A (Ogg/Theora)'
    size: '225M'
---

Copyright developed in the age of the printing press, and was designed to fit with the system of centralized copying imposed by the printing press. But the copyright system does not fit well with computer networks, and only draconian punishments can enforce it.

The global corporations that profit from copyright are lobbying for draconian punishments, and to increase their copyright powers, while suppressing public access to technology. But if we seriously hope to serve the only legitimate purpose of copyright -- to promote progress, for the benefit of the public -- then we must make changes in the other direction.

This talk by Richard M. Stallman is broken into two parts: the main talk and the question and answer sessions following the talk. Both are available in only Ogg/Theora format in keeping with Stallman's wishes. They are available under the [ Creative Commons NoDerivs 1.0](<http://creativecommons.org/licenses/nd/1.0/>) license.
