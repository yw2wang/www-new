---
index: 8
title: 'Back to Back Talks: Culture Turnaround and Software Defined Networks'
presentors:
  - John Stix
  - Francisco Dominguez
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/fibernetics-thumb-small.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/fibernetics.mp4'
    type: 'Talk (x264)'
---

Back to back talks from John Stix and Francisco Dominguez on turning a company's culture around and on Software Defined Networks!

John Stix will be talking about how he turned around the corporate culture at Fibernetics Corporation.

Francisco Dominguez will be talking about Software Defined Networks, which for example can turn multiple flakey internet connections into one reliable one.

The speakers are:

- John Stix - President, Fibernetics
- Francisco Dominguez - CTO, Fibernetics

<!-- -->

Food and drinks will be provided!
