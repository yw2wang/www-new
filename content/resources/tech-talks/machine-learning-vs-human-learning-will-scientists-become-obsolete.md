---
index: 23
title: 'Machine learning vs human learning: will scientists become obsolete'
presentors:
  - Dr. Shai Ben-David
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/human-vs-machine-learning-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/human-vs-machine-learning-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/human-vs-machine-learning.mp4'
    type: 'Talk (x264)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/human-vs-machine-learning.mpg'
    type: 'Talk (MPG)'
---
