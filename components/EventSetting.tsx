import React from "react";

import styles from "./EventSetting.module.css";

interface Props {
  startDate: Date;
  endDate?: Date;
  online: boolean;
  location: string;
}

export function EventSetting(props: Props) {
  const date = props.startDate.toLocaleDateString("en-US", {
    day: "numeric",
    month: "long",
    year: "numeric",
  });
  const time = props.startDate.toLocaleTimeString("en-US", {
    hour: "numeric",
    minute: "numeric",
    timeZoneName: "short",
  });

  const endDate =
    props.endDate?.toLocaleDateString("en-US", {
      day: "numeric",
      month: "long",
      year: "numeric",
    }) ?? "";

  const location = props.online ? `Online - ${props.location}` : props.location;
  const separator = <span className={styles.separator}> | </span>;

  return (
    <div className={styles.container}>
      {!props.endDate || date == endDate ? (
        // Single day event
        <>
          <time dateTime={props.startDate.toISOString()}>{date}</time>
          {separator}
          <span>{time}</span>
        </>
      ) : (
        // Multi day event
        <span>
          <time dateTime={props.startDate.toISOString()}>{date}</time>
          <span> - </span>
          <time dateTime={props.startDate.toISOString()}>{endDate}</time>
        </span>
      )}
      {separator}
      {location}
    </div>
  );
}
