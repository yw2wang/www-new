import React, { ReactNode } from "react";

import styles from "./Blockquote.module.css";

export interface Props {
  children: ReactNode;
}

export function Blockquote(props: Props) {
  return (
    <blockquote className={styles.blockquote}>{props.children}</blockquote>
  );
}
