import React from "react";

import styles from "./HorizontalLine.module.css";

export function HorizontalLine() {
  return <hr className={styles.line} />;
}
