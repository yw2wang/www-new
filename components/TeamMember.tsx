import React from "react";

import { Image } from "./Image";

import styles from "./TeamMember.module.css";

interface TeamMemberProps {
  name: string;
  role?: string;
  image: string;
}

export const TeamMember: React.FC<TeamMemberProps> = ({
  name,
  role = "",
  image,
}) => {
  return (
    <div className={styles.container}>
      <Image className={styles.img} src={image} alt={`Picture of ${name}`} />
      <div className={styles.caption}>
        <div className={styles.name}>{name}</div>
        <div className={styles.role}>{role}</div>
      </div>
    </div>
  );
};
