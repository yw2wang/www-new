import NextLink, { LinkProps as NextLinkProps } from "next/link";
import React from "react";

import styles from "./Link.module.css";

type Props = Omit<NextLinkProps, "href"> & { href: string };

export const Link: React.FC<Props> = (props) => {
  const { children, ...otherProps } = props;
  const { href } = otherProps;

  const isExternal = href.includes("http://") || href.includes("https://");

  return isExternal ? (
    <a
      className={styles.link}
      href={href}
      target="_blank"
      rel="noopener noreferrer"
    >
      {children}
    </a>
  ) : (
    <NextLink {...otherProps}>
      <a className={styles.link}>{children}</a>
    </NextLink>
  );
};
