import React, { ReactNode } from "react";

import { EventSetting } from "./EventSetting";
import { Link } from "./Link";

import styles from "./MiniEventCard.module.css";

interface MiniEventCardProps {
  name: string;
  description: ReactNode;
  short: string;
  online: boolean;
  location: string;
  startDate: Date;
  endDate?: Date;
  background: "dark-bg" | "normal-bg";
  year: number;
  term: string;
  slug: string;
}

export const MiniEventCard: React.FC<MiniEventCardProps> = ({
  name,
  short,
  description,
  location,
  startDate,
  endDate,
  online,
  background,
  year,
  term,
  slug,
}) => {
  const cardBackground =
    background === "dark-bg" ? `${styles.darkBg} ${styles.card}` : styles.card;
  return (
    <details className={cardBackground}>
      <summary>
        <div onClick={(event) => event.preventDefault()}>
          <h2 className={styles.name}>
            <Link href={`/events/${year}/${term}/${slug}`}>{name}</Link>
            <div className={styles.nameSpacer}></div>
          </h2>
          <div className={styles.info}>
            <EventSetting
              startDate={startDate}
              endDate={endDate}
              location={location}
              online={online}
            />
          </div>
          <p className={styles.shortDescription}>{short}</p>
        </div>
        <div className={styles.details}>View details {dropDownIcon}</div>
      </summary>
      <div>{description}</div>
    </details>
  );
};

const dropDownIcon = (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="14"
    height="9"
    viewBox="0 0 14 9"
    className={styles.dropDownIcon}
  >
    <path d="M6.24407 8.12713C6.64284 8.58759 7.35716 8.58759 7.75593 8.12713L13.3613 1.65465C13.9221 1.00701 13.4621 0 12.6053 0H1.39467C0.537918 0 0.0778675 1.00701 0.638743 1.65465L6.24407 8.12713Z" />
  </svg>
);
