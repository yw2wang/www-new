import { writeFile } from "fs/promises";

import { getMembers } from "@/lib/members";
import { getCurrentTermYear } from "@/utils";

async function createMembersApi() {
  const { term, year } = getCurrentTermYear();
  const members = await getMembers(year, term);

  const result = {
    $schema: "https://json-schema.org/draft/2020-12/schema",
    $id: "schema/members.json",
    members,
  };

  await writeFile("public/api/members.json", JSON.stringify(result));
}

void createMembersApi();
