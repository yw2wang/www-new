import Head from "next/head";
import React from "react";

export default function ResourcesRedirect() {
  return (
    <Head>
      <meta httpEquiv="refresh" content="0;url=/resources/services" />
    </Head>
  );
}
