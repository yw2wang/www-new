import Link from "next/link";
import { useRouter } from "next/router";
import React, { ReactNode } from "react";

import { Image } from "@/components/Image";
import { Title } from "@/components/Title";

import Content from "../../../content/resources/advice/co-op-advice.md";

import styles from "./co-op.module.css";

export default function CoopAdvice() {
  return (
    <>
      <Title>Co-op Advice</Title>
      <Advice>
        <Content />
      </Advice>
    </>
  );
}

export function Advice(props: { children: ReactNode }) {
  const router = useRouter();
  const path = router.pathname;
  return (
    <>
      <div className={styles.titleContainer}>
        <h1 className={styles.title}>Waterloo Undergraduate Advice</h1>
        <Image src="/images/advice.svg" className={styles.codey} />
      </div>
      <div className={styles.adviceBarContainer}>
        <Link href="/resources/advice/co-op">
          <a
            className={
              path == "/resources/advice/co-op" ? styles.currentAdvice : ""
            }
          >
            Co-op Advice
          </a>
        </Link>
        <Link href="/resources/advice/academic">
          <a
            className={
              path == "/resources/advice/academic" ? styles.currentAdvice : ""
            }
          >
            Academic Advice
          </a>
        </Link>
        <Link href="/resources/advice/misc">
          <a
            className={
              path == "/resources/advice/misc" ? styles.currentAdvice : ""
            }
          >
            Additional Resources
          </a>
        </Link>
      </div>
      <div className={styles.content}>{props.children}</div>
    </>
  );
}
