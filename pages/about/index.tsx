import React from "react";

import { ConnectWithUs } from "@/components/ConnectWithUs";
import { DefaultLayout } from "@/components/DefaultLayout";
import { EmailSignup } from "@/components/EmailSignup";
import { Image } from "@/components/Image";
import {
  GetShapesConfig,
  mobileShapesConfig,
} from "@/components/ShapesBackground";
import { Title } from "@/components/Title";

import Content from "../../content/about/index.mdx";

import styles from "./index.module.css";

export default function AboutUs() {
  return (
    <>
      <Title>About</Title>
      <div className={styles.titleContainer}>
        <h1 className={styles.title}>About Us!</h1>
        <Image src="/images/about-us.svg" className={styles.codey} />
      </div>
      <div className={styles.content}>
        <Content />
      </div>
      <DefaultLayout>
        <ConnectWithUs />
        <EmailSignup />
      </DefaultLayout>
    </>
  );
}

AboutUs.Layout = function AboutUsLayout(props: { children: React.ReactNode }) {
  return <div className={styles.page}>{props.children}</div>;
};

AboutUs.getShapesConfig = (() => {
  const desktopConfig = {
    cross: [
      {
        top: "calc(16rem / 16)",
        left: "calc(-78rem / 16)",
        width: "calc(150rem / 16)",
        height: "calc(150rem / 16)",
        transform: "rotate(30deg)",
      },
    ],
    dots: [
      {
        top: "calc(520rem / 16)",
        right: "calc(-120rem / 16)",
        width: "calc(292rem / 16)",
        height: "calc(330rem / 16)",
        transform: "rotate(-29deg)",
      },
    ],
    hash: [
      {
        top: "calc(528rem / 16)",
        left: "60vw",
        width: "calc(60rem / 16)",
        height: "calc(60rem / 16)",
      },
      {
        bottom: "calc(440rem / 16)",
        right: "84vw",
        width: "calc(60rem / 16)",
        height: "calc(60rem / 16)",
      },
    ],
    triangle: [
      {
        top: "calc(554rem / 16)",
        right: "80vw",
        width: "calc(68rem / 16)",
        height: "calc(68rem / 16)",
        transform: "rotate(-26deg)",
      },
      {
        top: "calc(2190rem / 16)",
        right: "4vw",
        width: "calc(68rem / 16)",
        height: "calc(68rem / 16)",
        transform: "rotate(-26deg)",
      },
    ],
    waves: [
      {
        top: "calc(1300rem / 16)",
        left: "2vw",
        width: "calc(102rem / 16)",
        height: "calc(50rem / 16)",
      },
    ],
    wavesBig: [
      {
        top: "calc(42rem / 16)",
        right: "calc(-160rem / 16)",
        width: "calc(376rem / 16)",
        height: "calc(132rem / 16)",
      },
      {
        bottom: "calc(40rem / 16)",
        left: "calc(-174rem / 16)",
        width: "calc(376rem / 16)",
        height: "calc(132rem / 16)",
      },
    ],
  };

  if (window.innerWidth <= 768) {
    return mobileShapesConfig;
  }
  return desktopConfig;
}) as GetShapesConfig;
