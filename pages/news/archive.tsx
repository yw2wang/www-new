import { GetStaticProps } from "next";

import { ArchivePage, Props } from "@/components/ArchivePage";
import { getNewsTermsByYear, getNewsYears } from "@/lib/news";

export default ArchivePage;

export const getStaticProps: GetStaticProps<Props> = async () => {
  const years = (await getNewsYears()).reverse();
  const yearsWithTerms = await Promise.all(
    years.map(async (year) => ({
      year,
      terms: (await getNewsTermsByYear(year)).reverse(),
    }))
  );

  return { props: { items: yearsWithTerms, type: "news" } };
};
