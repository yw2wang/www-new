import { ParsedUrlQuery } from "querystring";

import { GetStaticPaths, GetStaticProps } from "next";
import { MDXRemote } from "next-mdx-remote";
import React from "react";

import { NewsCard } from "@/components/NewsCard";
import {
  ShapesConfig,
  defaultGetShapesConfig,
  GetShapesConfig,
} from "@/components/ShapesBackground";
import { Title } from "@/components/Title";
import {
  getNewsBySlug,
  getNewsByTerm,
  getNewsTermsByYear,
  getNewsDateByTerm,
  getNewsYears,
  News,
} from "@/lib/news";
import { Term } from "@/utils";

import styles from "./[date].module.css";

interface Props {
  year: string;
  term: Term;
  news: News[];
}

export default function DateNews({ news }: Props) {
  const date = new Date(news[0].metadata.date).toLocaleDateString("en-US", {
    year: "numeric",
    month: "long",
    day: "numeric",
  });
  return (
    <div className={styles.page}>
      <Title>{["News", `${date}`]}</Title>
      <h1>News: {date}</h1>
      {news.map(({ content, metadata }, idx) => (
        <NewsCard
          key={idx}
          {...metadata}
          date={new Date(metadata.date)}
          fit={true}
        >
          <MDXRemote {...content} />
        </NewsCard>
      ))}
    </div>
  );
}

DateNews.getShapesConfig = ((width, height) => {
  return window.innerWidth <= 768
    ? ({} as ShapesConfig)
    : defaultGetShapesConfig(width, height);
}) as GetShapesConfig;

export const getStaticProps: GetStaticProps<Props, Params> = async (
  context
) => {
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  const { year, term, date } = context.params!;
  const slugs = (await getNewsByTerm(year, term)).filter((slug) =>
    slug.includes(date)
  );

  const news = await Promise.all(
    slugs.map((slug) => getNewsBySlug(year, term, slug))
  );
  // Reverse so that we are displaying the most recent news
  // of term first
  return { props: { year, term, news: news.reverse() } };
};

interface Params extends ParsedUrlQuery {
  year: string;
  term: Term;
  date: string;
}

export const getStaticPaths: GetStaticPaths<Params> = async () => {
  const years = await getNewsYears();
  const terms = await Promise.all(
    years.map(async (year) => {
      const termsInYear = await getNewsTermsByYear(year);
      return termsInYear.map((term) => ({ year, term }));
    })
  );
  const dates = await Promise.all(
    terms.map(async (termInYear) => {
      const datesInTerm: Params[] = [];
      for (const { year, term } of termInYear) {
        const dates = await getNewsDateByTerm(year, term);
        dates.map((date) => datesInTerm.push({ year, term, date }));
      }
      return datesInTerm.flat();
    })
  );

  return {
    paths: dates.flat().map((params) => ({ params })),
    fallback: false,
  };
};
