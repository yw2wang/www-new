import React from "react";

import { Image } from "@/components/Image";
import { Link } from "@/components/Link";
import { Title } from "@/components/Title";

import styles from "./404.module.css";

export default function Custom404() {
  return (
    <div className={styles.container}>
      <div className={styles.text}>
        <Title>404: Page Not Found</Title>
        <h2>Error 404</h2>
        <h1 className={styles.heading}>
          We couldn’t find the page you’re looking for!
        </h1>
        <p>
          <Link href="/">Click here to go back to the main page.</Link>
        </p>
      </div>

      <Image src="images/codey-404.svg" className={styles.codey} />
    </div>
  );
}
